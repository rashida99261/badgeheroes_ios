//
//  MissionFinishedResponse.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

import ObjectMapper

class MissionFinishedResponse: Mappable {
    
    var mission: Mission?
    var newTitles: [Title]?
    var newMedals: [Medal]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        mission <- map["mission"]
        newTitles <- map["new_titles"]
        newMedals <- map["new_medals"]
    }
    
}
