//
//  File.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/8/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import RealmSwift

open class BFirebaseSessionNotifier: BFirebaseNotifier {
    
    @objc dynamic var startDate: Int = -1
    @objc dynamic var endDate: Int = -1
    
    override func notify(_ callback: (() -> Void)?) {
        
        
        if endDate != -1 && startDate != -1{
            let duration = endDate - startDate
            firebase({ (ref) in
                let path = "clients/\(Settings.ClientId)/sessions"
                let autoKey = ref.child(path).childByAutoId().key
                let startDateNum = self.startDate
                let endDateNum = self.endDate
                let session = [
                    "uid"       :   Settings.UserId,
                    "team_id"   :   Settings.TeamId,
                    "start_date":   startDateNum,
                    "end_date"  :   endDateNum,
                    "duration"  :   duration
                ]
                let childUpdates = ["/\(path)/\(String(describing: autoKey))": session]
                ref.updateChildValues(childUpdates)
                
                callback?()
            })
        }
        
    }
    
    
    public static func push(_ startDate: Date, endDate: Date){
        let realm = try! Realm()
        let start = Int(startDate.timeIntervalSince1970)
        let end = Int(endDate.timeIntervalSince1970)
        
        let sessionNotifier = BFirebaseSessionNotifier()
        sessionNotifier.startDate = start
        sessionNotifier.endDate = end
        try! realm.write({
            realm.add(sessionNotifier)
        })
        
        let sessionNotifies = realm.objects(BFirebaseSessionNotifier.self)
        for sn in sessionNotifies{
            sn.notify({
                try! realm.write {
                    realm.delete(sn)
                }

            })
        }
        
        
    }
}
