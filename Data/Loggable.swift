//
//  Loggable.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

protocol Loggable {
    
    func logCompleted(_ me: Bool) -> String
    
    func logLevelUp(_ me: Bool) -> String
    
    func logRejected(_ me: Bool) -> String
    
    func logGetContent(_ state: HistoryLog.LogState, me: Bool) -> String
}
