//
//  DateParser.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import ObjectMapper



class DateParser: TransformType {
    typealias Object = Date
    typealias JSON = String
    var dateFormatter: DateFormatter
    
    required init() {
        dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
    }
    
    func transformFromJSON(_ value: Any?) -> Object? {
        if let timeInt = value as? Double {
            return Date(timeIntervalSince1970: TimeInterval(timeInt))
        }
        
        if let timeStr = value as? String {
            
            let date = dateFormatter.date(from: timeStr)
            return date
        }
        
        return nil
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        if let date = value {
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
        
        return nil
    }
    

}
