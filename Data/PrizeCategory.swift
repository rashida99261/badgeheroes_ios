//
//  PrizeCategory.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/5/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import ObjectMapper

class PrizeCategory: Mappable, BCategory {
    
    var id: Int?
    var name: String?
    var color: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id <- map["id"]
        color <- map["color"]
        name <- map["name"]
    }
    
}
