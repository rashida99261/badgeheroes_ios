//
//  Campaign.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

import ObjectMapper

open class Campaign: Mappable, BCategorizable {
    
    var id: Int?
    var name: String?
    var imageUrl: String?
    var campaignCategoryId: Int?
    var serverState: Int?
    var totalMissions: Int?
    var completedMissions: Int?
    var position: Int?
    var color: String?
    var missions: [Mission]?
    var row: Int?
    var col: Int?
    var fullMode: Bool?
    var beginDate: Date?
    var endDate: Date?
    var completed: Bool?
    var hierarchyType: Int?
    
    
    var internalState: CampaignState = CampaignState.available
    
    static let SECONDS_IN_ONE_DAY = 24*60*60
    static let SECONDS_IN_ONE_HOUR = 60*60
    static let SECONDS_IN_ONE_MINUTE = 60
    
    
    var categoryId: Int{
        get{
            if let cid = campaignCategoryId{
                return cid
            }
            return -1
        }
    }
    
    var Completed: Bool{
        get{
            if let completed = completed{
                return completed
            }
            return false
        }
    }
    
    
    var BeginDateMessage: String{
        if let beginDate = self.beginDate{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "'Esta campaña estará disponible el 'dd-MM-yyyy"
            
            let ret = dateFormatter.string(from: beginDate)
            return ret
        }else{
            return "Esta campaña ya no está disponible"
        }
    }
    
    var RemainingTimeMessage: String{
        var message = ""
        if let endDate = self.endDate{
            let current = Date()
            
            var diffSeconds = Int(endDate.seconds(from: current))
            
            if diffSeconds > 0{
                
                let days = NSNumber(value: diffSeconds/Campaign.SECONDS_IN_ONE_DAY)
                diffSeconds = diffSeconds%Campaign.SECONDS_IN_ONE_DAY
                let hours = NSNumber(value: diffSeconds/Campaign.SECONDS_IN_ONE_HOUR)
                diffSeconds = diffSeconds%Campaign.SECONDS_IN_ONE_HOUR
                let minutes = NSNumber(value: diffSeconds/Campaign.SECONDS_IN_ONE_MINUTE)
                diffSeconds = diffSeconds%Campaign.SECONDS_IN_ONE_MINUTE
                let seconds = NSNumber(value: diffSeconds)
                
                if days.floatValue > 0.0{
                    message = "\(days)d "
                }
                
                let formatter = NumberFormatter()
                formatter.minimumIntegerDigits = 2
                
                message = "\(message)\(formatter.string(from: hours)!):\(formatter.string(from: minutes)!):\(formatter.string(from: seconds)!)"
                
            }
//            if diffDays > 1{
//                message = "\(diffDays)d "
//            }
//            
//            if diffSeconds > 0 {
//                let days = diffDays/SECONDS_IN_ONE_DAY
//                let hours = (diffSeconds - days*24*60*60)/(60*60)
//                let minutes = (diffSeconds - days*24*60*60 - hours*60*60)/60
//                let seconds = (diffSeconds - days*24*60*60 - hours*60*60 - minutes*60)/1
//                message = "\(message)\(hours):\(minutes):\(seconds)"
//            }
        }
        return message
    }
    
    var Position: Int{
        get{
            if let p = position{
                return p
            }
            return 0
        }
    }
    
    var Row: Int{
        get{
            if let p = row{
                return p
            }
            return 0
        }
    }
    
    var Col: Int{
        get{
            if let p = col{
                return p
            }
            return 0
        }
    }
    
    var FullMode: Bool{
        get{
            if let p = fullMode{
                return p
            }
            return false
        }
    }
    
    public enum CampaignState : Int{
        case available = 0
        case locked = 1
        case expired  = 2
        case completed  = 3
        case notFound  = -1
    }
    
    public enum CampaignHierarchyType : Int{
        case required = 0
        case bonus = 1
        case notFound = -1
    }
    
    
    var HierarchyType: CampaignHierarchyType{
        get{
            if let hierarchyType = self.hierarchyType{
                if let enumState = CampaignHierarchyType.init(rawValue: hierarchyType){
                    return enumState
                }
            }
            return CampaignHierarchyType.notFound
            
        }
    }
    
    
    var State: CampaignState{
        get{
            if let serverState = self.serverState{
                if internalState == CampaignState.available{
                    if let enumState = CampaignState.init(rawValue: serverState){
                        return enumState
                    }
                }else{
                    return internalState
                }
            }
            return CampaignState.notFound
        }
    }
    
    
    var Color: UIColor{
        get{
            if let c = self.color{
                return UIColor(hexString: c)!
            }
            return BColor.PrimaryBar
        }
    }
    required public init?(map: Map){
        
    }
    
    open func mapping(map: Map){
        id  <- map["id"]
        name  <- map["name"]
        imageUrl  <- map["image_url"]
        campaignCategoryId <- map["campaign_category_id"]
        serverState <- map["state"]
        totalMissions <- map["total_missions"]
        completedMissions  <- map["completed_missions"]
        color <- map["color"]
        missions <- map["missions"]
        position <- map["position"]
        row <- map["row"]
        col <- map["col"]
        beginDate <- (map["begin_date"], DateParser())
        endDate <- (map["end_date"], DateParser())
        fullMode <- map["full_mode"]
        hierarchyType <- map["hierarchy_type"]
        completed <- map["completed"]
        
    }
}
