//
//  Title.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class Title: Mappable, Loggable{
    
    var id: Int?
    var name: String?
    var imageUrl: String?
    var active: Bool?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
        imageUrl <- map["image_url"]
        active <- map["active"]
    }
    
    func logCompleted(_ me: Bool) -> String{
        if(me){
            return "¡El título \"\(name!)\" es tuyo!";
        }else{
            return " ha ganado el título \"\(name!)\"";
        }
    }
    
    func logLevelUp(_ me: Bool) -> String{
        if(me){
            return "Ganaste el título \"\(name!)\"";
        }else{
            return " ha ganado el título \"\(name!)\"";
        }
    }
    
    func logRejected(_ me: Bool) -> String{
        if(me){
            return "No has ganado \"\(name!)\"";
        }else{
            return " no ganó \"\(name!)\"";
        }
    }
    
    func logGetContent(_ state: HistoryLog.LogState, me: Bool) -> String{
        switch (state) {
        case .Completed:
            return logCompleted(me);
        case .LevelUp:
            return logLevelUp(me);
        case .Rejected:
            return logRejected(me);
        default:
            return "";
        }
    }
    
}
