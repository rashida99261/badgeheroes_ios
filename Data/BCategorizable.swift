//
//  BCategorizable.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

protocol BCategorizable {
    var categoryId: Int {get}
}