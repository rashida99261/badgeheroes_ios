//
//  PrizesResponse.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/5/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

import ObjectMapper

class PrizesResponse: Mappable {
    
    var prizes: [Prize]?
    var categories: [PrizeCategory]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        prizes <- map["prizes"]
        categories <- map["categories"]
    }
    
}
