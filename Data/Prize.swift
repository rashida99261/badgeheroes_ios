//
//  Prize.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/5/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

open class Prize: Mappable, BCategorizable{
    
    var id: Int?
    var prizeCategoryId: Int?
    var name: String?
    var imageUrl: String?
    var imageUrls: [String]?
    var color: String?
    var activationDate: Date?
    var expirationDate: Date?
    var credits: Int?
    var amount: Int?
    var description: String?
    var state: Int?
    var State: PrizeState{
        get{
            if let state = self.state{
                if let enumState = PrizeState.init(rawValue: state){
                    return enumState
                }
            }
            return PrizeState.notFound
        }
    }
    
    public enum PrizeState : Int{
        case waiting    = 0
        case accepted   = 1
        case rejected   = 2
        case notFound   = -1
    }
    
    
    
    var categoryId: Int{
        get{
            if let cid = prizeCategoryId{
                return cid
            }
            return -1
        }
    }
    
    required public init?(map: Map){
        
    }
    
    
    open func mapping(map: Map){
        id <- map["id"]
        prizeCategoryId <- map["prize_category_id"]
        name <- map["name"]
        imageUrl <- map["image_url"]
        imageUrls <- map["image_urls"]
        color <- map["color"]
        activationDate <- (map["activation_date"], DateParser())
        expirationDate <- (map["expiration_date"], DateParser())
        credits <- map["credits"]
        amount <- map["amount"]
        description <- map["description"]
        state <- map["state"]
    }
    
  
    
    
}
