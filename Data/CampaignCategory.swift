//
//  CampaignCategory.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import ObjectMapper

class CampaignCategory: Mappable, BCategory {
    
    var id: Int?
    var name: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id <- map["id"]
        name <- map["name"]
    }
    
}
