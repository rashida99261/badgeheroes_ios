//
//  CampaignTree.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 10/20/17.
//  Copyright © 2017 MisPistachos. All rights reserved.
//

import Foundation

import ObjectMapper

class CampaignTree: Mappable {
    
    var hierarchy: Bool?
    
    var Hierarchy: Bool{
        if let h = hierarchy{
            return h
        }
        return false
    }
  
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        hierarchy <- map["hierarchy"]
    }
    
    
    
}
