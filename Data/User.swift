//
//  User.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper

class User: Mappable {

    var id: Int?
    var clientId: Int?
    var teamId: Int?
    var firstName: String?
    var imageUrl: String?
    var lastName: String?
    var email: String?
    var accessToken: String?
    var firebaseAccessToken: String?
    var stagingMode: Bool?
    
    var username: String?
    var error_info: String?
    var avatar: String?
    var medals: [Medal]?
    var missions: [Mission]?
    var team: Team?
    var level: Int?
    var credits: Int?
    var experience: Int?
    var titles: [Title]?
    var title: Title?
    var titleId: Int?
    var user_ranking : [UserRanking]?
    
    var BTitle: Title?{
        get{
            
            if let titles = self.titles{
                for t in titles{
                    if let active = t.active , active == true{
                        return t
                    }
                }
            }
            return nil
        }
    }

    var Valid: Bool{
        get{
            if let _id = self.id{
                return _id > 0
            }
            return false
        }
    }
    
    var FullName: String{
        get{
            if let fName = firstName, let lName = lastName{
                return "\(fName) \(lName)".trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            }else{
                return ""
            }
        }
    }
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        id <- map["id"]
        firstName <- map["first_name"]
        imageUrl <- map["image_url"]
        lastName <- map["last_name"]
        email <- map["email"]
        accessToken <- map["access_token"]
        firebaseAccessToken <- map["firebase_access_token"]
        username <- map["username"]
        avatar <- map["avatar"]
        medals <- map["medals"]
        missions <- map["missions"]
        team <- map["team"]
        level <- map["level"]
        credits <- map["credits"]
        experience <- map["experience"]
        titles <- map["titles"]
        title <- map["title"]
        titleId <- map["title_id"]
        clientId <- map["client_id"]
        teamId <- map["team_id"]
        stagingMode <- map["staging_mode"]
        error_info <- map["error_info"]
        user_ranking <- map["ranking"]
    }
}
