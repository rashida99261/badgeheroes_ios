//
//  ArrayExtension.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/9/17.
//  Copyright © 2017 MisPistachos. All rights reserved.
//

import Foundation

extension Array {
    func group<T>(by criteria: (Element) -> T) -> [T: [Element]] {
        var groups = [T: [Element]]()
        for element in self {
            let key = criteria(element)
            if groups.keys.contains(key) == false {
                groups[key] = [Element]()
            }
            groups[key]?.append(element)
        }
        return groups
    }
}
