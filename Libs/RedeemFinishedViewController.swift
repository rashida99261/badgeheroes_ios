//
//  RedeemFinishedViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/19/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import SwiftEventBus

var topBarCredit : String = ""

class RedeemFinishedViewController: UIViewController{
    
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var topCreditLbl: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
 
    @IBOutlet weak var mainMessage: UILabel!
    @IBOutlet weak var complementMessage: UILabel!
    @IBOutlet weak var continueButton: BSecondaryButton!
    var success: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reloadLogo()
        
        self.navBarView.backgroundColor = BColor.PrimaryBar
        topCreditLbl.text = topBarCredit
        
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
        
        if self.success{
            self.toSuccessPrizeRedeem()
        }else{
            self.toFailPrizeRedeem()
        }
    }
    
    
    @IBAction func continueButtonPressed(_ sender: AnyObject) {
        let controllers: [AnyObject] = self.navigationController!.viewControllers
        for controller in controllers{
            
            if (controller is PrizesViewController) {
                self.dismiss(animated: true, completion: nil)
                self.navigationController!.popToViewController(controller as! UIViewController, animated: true)
                return
                
            }
        }
        
    }
    
    fileprivate func toFailPrizeRedeem(){
        //self.mainMessage.text = "No tienes suficientes créditos"
       // self.complementMessage.text = "No se ha podido canjear tu premio"
       // self.continueButton.setTitle("CERRAR Y CONTINUAR", for: UIControl.State())
    }
    
    fileprivate func toSuccessPrizeRedeem(){
        self.mainMessage.text = "¡Premio Canjeado!"
        self.complementMessage.text = "¡Felicitaciones! Tu premio está siendo validado."
       //self.continueButton.setTitle("CERRAR Y CONTINUAR", for: UIControl.State())
    }
    
    
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
