//
//  RegardPopupController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class RewardPopupController: BModalViewController {
    
   // @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var imageView: UIImageView!
  //  @IBOutlet weak var creditsLabel: UILabel!
   // @IBOutlet weak var creditsIcon: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
 //   @IBOutlet var backgroundView: UIView!
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var marginTopConstraint: NSLayoutConstraint!
    var popupColor = BColor.Light //UIColor(patternImage: UIImage(named: "yellow_background")!)
    
    var imageUrl: String!
    var credits: Int!
    var fromLocal = false
    var titleMessage: String!
    
    var modalCanceled: (() -> Void)?
    
    var originalMarginTopConstant: CGFloat = 0
    
    override func viewDidLoad() {
        self.modalPresentationStyle = .overCurrentContext
        self.view.backgroundColor = UIColor.clear
        //self.popupView.backgroundColor = popupColor
       // self.titleLabel.text = titleMessage
        if let u = imageUrl{
            if fromLocal{
                self.imageView.image = UIImage.init(named: u)
            }else{
                self.imageView.kf.setImage(with: URL(string: u))
            }
            
        }
        
//        if credits > 0{
//            self.creditsLabel.text = "+ \(credits!) créditos"
//        }else{
//            self.creditsIcon.isHidden = true
//            self.creditsLabel.isHidden = true
//        }
//        
//        self.popupView.layer.cornerRadius = 10
//        self.popupView.layer.masksToBounds = true
//        
//        backgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
//        self.view.bringSubviewToFront(backgroundView)
        
    }
    
    @IBAction func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
        self.modalCanceled?()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 5, delay: 0.0, options: UIView.AnimationOptions(), animations:  {
               // self.marginTopConstraint.constant = self.originalMarginTopConstant
        }, completion: nil)
        
    }
    
    @IBAction func continueButtonPressed(_ sender: AnyObject) {
        
        self.modalFinished?()
    }
    
    

}
