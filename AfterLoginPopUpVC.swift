

import UIKit

class AfterLoginPopUpVC: BModalViewController {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblMsg : UILabel!
    @IBOutlet weak var btnContinuar : UIButton!
    
    var credits: Int!
    var fromLocal = false
    var strTitle: String!
    
    var modalCanceled: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.lblTitle.text = strTitle.uppercased()
        self.lblMsg.text = "Te damos la bienvenida.¡Esperamos que te entretengas con nuestras"
        
        btnContinuar.layer.cornerRadius = 25
        btnContinuar.clipsToBounds = true
        btnContinuar.backgroundColor = GlobalBgColor?.darker(by: 13)
    }
    

    @IBAction func backgroundViewTapped(_ sender: UITapGestureRecognizer) {
        self.modalCanceled?()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 5, delay: 0.0, options: UIView.AnimationOptions(), animations:  {
            // self.marginTopConstraint.constant = self.originalMarginTopConstant
        }, completion: nil)
        
    }
    
    @IBAction func continueButtonPressed(_ sender: AnyObject) {
        
        self.modalFinished?()
    }
}
