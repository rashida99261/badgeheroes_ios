//
//  BAnswerCell.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 3/21/17.
//  Copyright © 2017 MisPistachos. All rights reserved.
//

import UIKit

class BAnswerCell: UITableViewCell {
    
    var answerId: Int!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var btnVerMAs: UIButton!
    
    @IBOutlet weak var comment: UILabel!
    
}
