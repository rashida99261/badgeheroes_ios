//
//  GeneralContactSelectorView.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 11/8/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit


class GeneralContactTypeSelectorViewController: BModalViewController{
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var reportButton: BSecondaryButton!
    @IBOutlet weak var contactButton: BSecondaryButton!
    
    override func viewDidLoad() {
        self.userNameLabel.text = Settings.FullName
        if Settings.ImageUrl.count > 0 {
            userImage.kf.setImage(with: URL(string: Settings.ImageUrl)!)
        }
        self.view.backgroundColor = BColor.SecondaryBar
        UIUtils.toCircleImage(userImage)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.contactButton.unlockButton()
        self.reportButton.unlockButton()
//        self.reportButton.backgroundColor = UIColor(hexString: "#FAB40A")
//        self.reportButton.setTitleColor(UIColor(hexString: "#FFFFFF"), for: .normal)
//        self.contactButton.backgroundColor = UIColor(hexString: "#FAB40A")
//        self.contactButton.setTitleColor(UIColor(hexString: "#FFFFFF"), for: .normal)
    }
    
    
    @IBAction func reportButtonPressed(_ sender: AnyObject) {
        loadNextView(contactType: UsersAPI.CommentType.problem)
    }
    
    @IBAction func contactButtonPressed(_ sender: AnyObject) {
        loadNextView(contactType: UsersAPI.CommentType.other)
    }
    
    
    func loadNextView(contactType: UsersAPI.CommentType){
        let contactController = self.storyboard!.instantiateViewController(withIdentifier: "GeneralContactViewController") as! GeneralContactViewController
       // contactController.modalFinished = self.modalFinished
       // contactController.commentType = contactType
        UIUtils.showInDefaultModal(self, controller: contactController)
        
        
    }
}
