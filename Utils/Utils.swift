//
//  Utils.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//
import SwiftyUserDefaults
import Alamofire

 class Utils {
    
    
    static func dateToString(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy' a las 'hh:mm"
        let dateString = dateFormatter.string(from: date)
        return dateString
       
    }
    
    static func downloadFile(_ url: String, callback: ((URL)-> Void)?){
        
        
        
    var localPath: URL?
        
        
//        Alamofire.download(<#T##url: URLConvertible##URLConvertible#>, method: <#T##HTTPMethod#>, parameters: <#T##Parameters?#>, encoding: <#T##ParameterEncoding#>, headers: <#T##HTTPHeaders?#>, to: <#T##DownloadRequest.DownloadFileDestination?##DownloadRequest.DownloadFileDestination?##(URL, HTTPURLResponse) -> (destinationURL: URL, options: DownloadRequest.DownloadOptions)#>)
        
        Alamofire.download(url, method: .get, parameters: nil, encoding: Alamofire.URLEncoding() as ParameterEncoding, headers: nil, to: { (temporaryURL, response) in
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let pathComponent = response.suggestedFilename
            
            localPath = directoryURL.appendingPathComponent(pathComponent!)
            
            
            let fileManager = FileManager.default
            
            do {
                
                try fileManager.removeItem(atPath: localPath!.absoluteString)
                
            }
            catch let error as NSError {
                print("Ooops! Something went wrong: \(error)")
            }
            
            return (localPath!, [.removePreviousFile, .createIntermediateDirectories])
        }).response(completionHandler: { (response) in
            print(response)
            print("Downloaded file to \(localPath!)")
            if let cbk = callback{
                cbk(localPath!)
            }
        })
            
            
        
            
       
//        Alamofire.download(url,method: .get,
//            to: { (temporaryURL, response) in
//                let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//                let pathComponent = response.suggestedFilename
//                
//                localPath = directoryURL.appendingPathComponent(pathComponent!)
//                
//               
//                let fileManager = FileManager.default
//                
//                do {
//                    
//                    try fileManager.removeItem(atPath: localPath!.absoluteString)
//                    
//                }
//                catch let error as NSError {
//                    print("Ooops! Something went wrong: \(error)")
//                }
//                
//                return localPath!
//                
//        }).response { (request, response, _, error) in
//            print(response)
//            print("Downloaded file to \(localPath!)")
//            if let cbk = callback{
//                cbk(localPath!)
//            }
        
//        }
        
        
       
 
    }
  
    
}

