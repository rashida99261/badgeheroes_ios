//
//  BFirebase.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/5/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import RealmSwift

class BFirebase {
    
    
    enum ViewCode: Int {
        case campaigns = 0
        case activity = 1
        case profile = 2
        case market = 3
        
    }
    
    static func pushViewPressed(_ viewCode: ViewCode){
        
        
        
        BFirebaseClickNotifier.push(viewCode.rawValue, date: Date())
        
        
        
    }
    
    static func pushSessionFinished(_ startDate: Date, endDate: Date){
        BFirebaseSessionNotifier.push(startDate, endDate: endDate)
    }
    
    
    
    
    
    
    
}
