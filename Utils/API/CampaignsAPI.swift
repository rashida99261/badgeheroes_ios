//
//  CampaignsAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import Foundation

open class CampaignsAPI: BaseAPI{
    
    static func getCampaigns(userId: Int, callback: @escaping (CampaignsResponse?, NSError?) -> Void){
        let id = String(userId) as AnyObject
        CampaignsAPI.get("campaigns", parameters: ["id": id], callback)
    }
    
    
    static func getCampaign(campaignId: Int, callback: @escaping (Campaign?, NSError?) -> Void){
        
        CampaignsAPI.get("campaigns/\(String(campaignId))", parameters: [String: AnyObject](), callback)
    }
    
    static func postSubscribe(reason: String, callback: @escaping (ApiResponse?, NSError?) -> Void){
        CampaignsAPI.post("campaigns/subscribe", parameters: ["reason": reason as AnyObject], callback)
    }
    
    
}
