//
//  MedalAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/4/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

open class MedalsAPI: BaseAPI{
    
    
    static func getMedal(medalId: Int, callback: @escaping (Medal?, NSError?) -> Void){
        MedalsAPI.get("medals/\(String(medalId))", parameters: [String: AnyObject](), callback)
    }
}
