//
//  ClientsAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

open class ClientsAPI: BaseAPI{
    
    static func getSettings(_ callback: @escaping (BConfig?, NSError?) -> Void){
        
        ClientsAPI.get("clients/settings" , parameters: [String:String]() as [String : AnyObject], callback)
    }
    
   
    
}
