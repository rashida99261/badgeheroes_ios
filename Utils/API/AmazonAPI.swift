//
//  AmazonAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import AWSS3
import Foundation


open class AmazonAPI: BaseAPI{
    
    static let bucketName: String = "badgeheroes";
    static let folderName: String = "ios_temporal/";
    
    static func o(){
        
    }
    
    
    //los parametros de progressCallback son (bytesSent, totalBytesSent, totalBytesExpectedToSend)
    public static func uploadFile(_ fileName: String, data: Data, progressCallback: @escaping (Int64, Int64, Int64) -> Void, finishCallback: @escaping (NSError?, String?, URL?) -> Void){
        let tempFile = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("temp")
        try? data.write(to: tempFile, options: [.atomic])
        
        let expression = AWSS3TransferUtilityUploadExpression()
        expression.progressBlock = {(task, progress) in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Update a progress bar.
            })
        }
        
        var completionHandler: AWSS3TransferUtilityUploadCompletionHandlerBlock?
        completionHandler = { (task, error) -> Void in
            DispatchQueue.main.async(execute: {
                // Do something e.g. Alert a user for transfer completion.
                // On failed uploads, `error` contains the error object.
            })
        }
        
        let transferUtility = AWSS3TransferUtility.default()
        transferUtility.uploadData(data,
                                   bucket: AmazonAPI.bucketName,
                                   key: "ios_temporal/\(fileName)",
                                   contentType: "text/plain",
                                   expression: expression,
                                   completionHandler: completionHandler).continueWith {
                                    (task) -> AnyObject? in
                                    var s3URL: URL?
                                    let exception = task.description
                                    let error = task.error
                                    if let error = task.error {
                                        print("Error: \(error.localizedDescription)")
                                        s3URL = nil
                                    }
                                    
                                    if let _ = task.result {
                                        // Do something with uploadTask.
                                        s3URL = URL(string: "http://s3.amazonaws.com/\(self.bucketName)/\("ios_temporal/\(fileName)")")!
                                        print("Uploaded to:\n\(s3URL!)")
                                    }
                                    finishCallback(error as NSError?,exception,s3URL)
                                    return nil;
        }
        
//        if let uploadRequest = AWSS3TransferManagerUploadRequest(){
//            uploadRequest.key = "ios_temporal/\(fileName)"
//            uploadRequest.bucket = AmazonAPI.bucketName
//            uploadRequest.body = tempFile
//            uploadRequest.acl = AWSS3ObjectCannedACL.publicRead
//            uploadRequest.uploadProgress = progressCallback
//
//            let transferManager = AWSS3TransferManager.default()
//
//            transferManager.upload(uploadRequest).continueWith( block: { (task) -> AnyObject? in
//                let error = task.error
//                let exception = task.description
//                let s3URL: URL?
//                if task.result != nil {
//                    s3URL = URL(string: "http://s3.amazonaws.com/\(self.bucketName)/\(uploadRequest.key!)")!
//                    print("Uploaded to:\n\(s3URL!)")
//                }else{
//                    s3URL = nil
//                }
//                finishCallback(error as NSError?,exception,s3URL)
//                return nil
//            })
//        }
//    }
    }
    
}
