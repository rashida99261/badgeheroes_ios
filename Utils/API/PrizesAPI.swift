//
//  PrizesAPI.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

open class PrizesAPI: BaseAPI{
    
    static func getPrizes(userId: Int, callback: @escaping (PrizesResponse?, NSError?) -> Void){
        let id = String(userId) as AnyObject
        
        PrizesAPI.get("prizes", parameters: ["id": id], callback)
    }
    
    static func getPrize(_ prizeId: Int, callback: @escaping (Prize?, NSError?) -> Void){
        PrizesAPI.get("prizes/\(prizeId)", parameters: [String: AnyObject](), callback)
    }
    
    static func redeemPrize(_ prizeId: Int, callback: @escaping (ApiResponse?, NSError?) -> Void){
        PrizesAPI.post("prizes/\(prizeId)/redeem", parameters: [String: AnyObject](), callback)
    }
    
    static func myPrizes(_ callback: @escaping ([Prize]?, NSError?) -> Void){
        PrizesAPI.get("prizes/my_prizes", parameters: [String: AnyObject](), callback)
    }
    
    
}
