//
//  BFirebaseQueue.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/8/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import RealmSwift
import FirebaseDatabase
import FirebaseAuth

open class BFirebaseNotifier: Object {
    
    
    func notify(_ callback: (() -> Void)?) {
        
    }
    
   
    
    func firebase(_ function: @escaping ((_ ref: DatabaseReference) -> Void), attempts:Int = 2){
        
        if attempts == 0{
            return
        }
        
        Auth.auth().signIn(withCustomToken: Settings.FirebaseAccessToken, completion: { (user, error) in
            
            if error != nil {
                UsersAPI.updateFirebaseToken({ (user, error2) in
                    if error2 == nil{
                        if let fat = user?.firebaseAccessToken{
                            Settings.FirebaseAccessToken = fat
                        }
                        
                        if let tid = user?.teamId{
                            Settings.TeamId = tid
                        }
                        
                        if let cid = user?.clientId{
                            Settings.ClientId = cid
                        }
                        self.firebase(function, attempts: attempts - 1)
                    }
                })
            }
            if let _ = user{
//                print(u.uid)
                
                function(Database.database().reference())
            }
            
            
            
        })
        
        
        //        function(ref: FIRDatabase.database().reference())
        
        
    }
}
