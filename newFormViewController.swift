//
//  newFormViewController.swift
//  Badgeheroes
//
//  Created by Reinforce on 21/09/19.
//  Copyright © 2019 MisPistachos. All rights reserved.
//

import UIKit

class newFormViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    var missionId: Int!
    var missionPage: MissionPage!
    var elements: [PageElement]!
    var pagePosition: Int!
    var pageView: UIView!
    var watchRightAnswers: Bool! = false
    var watchUserAnswers: Bool! = false
    
    
    @IBOutlet weak var tblQue : UITableView!
    @IBOutlet weak var tblHeighnt : NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.elements = missionPage.pageElements
        self.tblQue.tableFooterView = UIView()
        self.tblQue.reloadData()
        
        
        for e in 0...(self.elements.count-1){
            
            let pe = self.elements[e]
            print(pe.QuestionType)
            
           
           // switch(){
                
//            case .Select:
//                let row = BtableView.dequeueReusableCell(withIdentifier: "BListSelectionRow", for: indexPath) as! BListSelectionRow
//                row.pageElement = pe
//
//                row.buttonPressed = {
//                    let optionMenu = row.getSelectOptionDialog()
//                    self.present(optionMenu, animated: true, completion: nil)
//                }
//                item = row
//            case .Radio, .Checkbox:
//                let row = BtableView.dequeueReusableCell(withIdentifier: "BMultipleSelectRow", for: indexPath) as! BMultipleSelectRow
//                row.pageElement = pe
//                row.hideKeyboard = self.dismissKeyboard
//                row.watchRightAnswers = self.watchRightAnswers
//                row.watchUserAnswers = self.watchUserAnswers
//                row.tableView.reloadData()
//                row.loadViews()
//
//                item = row
//
//            case .Input:
//                let row = BtableView.dequeueReusableCell(withIdentifier: "BTextFieldRow", for: indexPath) as! BTextFieldRow
//                row.pageElement = pe
//                row.textField.delegate = self
//                item = row
                
                
                
                
                
//            case .TextArea:
//                let row = BtableView.dequeueReusableCell(withIdentifier: "BTextAreaRow", for: indexPath) as! BTextAreaRow
//                row.pageElement = pe
//
//                row.textView.delegate = self
//                row.textView.isUserInteractionEnabled = true
//                row.textView.isEditable = true
//                row.textView.text = placeholder
//                row.textView.textColor = UIColor.lightGray
//                item = row
//
//            case .Image:
//                let row = BtableView.dequeueReusableCell(withIdentifier: "BImageRow", for: indexPath) as! BImageRow
//                row.pageElement = pe
//                row.imageButton.kf.setImage(with: URL(string: pe.mediaUrl!)!)
//                row.imagePressed = self.showImageModal
//                item = row
//
//            case .Video:
//                let row = BtableView.dequeueReusableCell(withIdentifier: "BVideoRow", for: indexPath) as! BVideoRow
//                row.pageElement = pe
//                row.controller = self
//                row.loadVideo()
//                item = row
//
                
           // default:
           //     print("question type")
           // }
        }
        
        // Do any additional setup after loading the view.
    }
    

    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.elements.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell()
        let pe = self.elements[indexPath.row]
        switch pe.QuestionType {
        case .Message:
            
            let cell = tblQue.dequeueReusableCell(withIdentifier: "messageCell") as! messageCell
            cell.labelmsgView.text = pe.label!.htmlToString
            return cell
            
        case .Input:
            let row = tblQue.dequeueReusableCell(withIdentifier: "BTextFieldRow") as! BTextFieldRow
            row.pageElement = pe
            return row
            
        case .TextArea:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "textAreaCell") as! textAreaCell
            cell.pageElement = pe
            cell.textView.delegate = self
            cell.textView.textColor = UIColor.lightGray
            return cell
            
            
        case .Image:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BImageRow") as! BImageRow
            cell.pageElement = pe
            cell.imageButton.kf.setImage(with: URL(string: pe.mediaUrl!)!)
            cell.imagePressed = self.showImageModal
            return cell
            
        case .Video:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BVideoRow") as! BVideoRow
            cell.pageElement = pe
            cell.controller = self
            cell.loadVideo()
            return cell
        
        case .Select:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BListSelectionRow") as! BListSelectionRow
            cell.pageElement = pe
            
            cell.buttonPressed = {
                let optionMenu = cell.getSelectOptionDialog()
                self.present(optionMenu, animated: true, completion: nil)
            }
            return cell
            
        case .Radio, .Checkbox:
            let cell = tblQue.dequeueReusableCell(withIdentifier: "BMultipleSelectRow") as! BMultipleSelectRow
            cell.pageElement = pe
            cell.watchRightAnswers = self.watchRightAnswers
            cell.watchUserAnswers = self.watchUserAnswers
            self.tblHeighnt.constant = self.tblQue.contentSize.height
            cell.tableView.reloadData()
            cell.loadViews()
            return cell
            
        default:
            print("")
        }
        
        self.tblHeighnt.constant = self.tblQue.contentSize.height
        return cell
    }
 
    func showImageModal(_ imageView: UIImageView){
        let fullscreenController = self.storyboard!.instantiateViewController(withIdentifier: "FullscreenImageViewController") as! FullscreenImageViewController
        fullscreenController.image = imageView.image
        fullscreenController.modalFinished = {
            self.dismiss(animated: false, completion: nil)
        }
        self.present(fullscreenController, animated: false, completion: nil)
    }
    
    
    
    
   
}

extension newFormViewController : MissionPageController{
    func completeMissionPage() -> MissionPage {
        return missionPage
    }

    func isPageCompleted() -> Bool {
        return true
    }

    func isPageCorrect() -> Bool {
        return true
    }

    func showErrorMessage() {
        
    }

    func loadDefault() {
        
    }
}

extension newFormViewController : UITextFieldDelegate, UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = "Escribe tu respuesta aquí"
            textView.textColor = UIColor.black
        }
    }
    
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        if(text == "\n") {
//            textView.resignFirstResponder()
//            return false
//        }
//        return true
//    }
}


class messageCell: UITableViewCell {
    
    @IBOutlet weak var labelmsgView: UILabel!
    @IBOutlet weak var labelmsgContainerView: UIView!
}

class textAreaCell: BBaseRow{
    
    @IBOutlet weak var textView: UITextView!

     override func completePage() {
        let ans = self.textView.text
        if ans != "" && ans != "Escribe tu respuesta aquí"{
            if self.pageElement != nil{
                self.pageElement.answerText = textView.text
                self.pageElement.isAnswered = true
            }
        }
    }
    
    override func loadDefault(){
        if let ans = pageElement.answerText{
            self.textView.text = ans
        }
    }
    
}
