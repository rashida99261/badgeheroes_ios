//
//  MissionsViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import  UIKit
import NVActivityIndicatorView

class MissionsViewController: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var campaignImage: UIImageView!
    @IBOutlet weak var campaignName: UILabel!
    
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var navBarViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var leadConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgViewBg: UIView!
    @IBOutlet weak var imgWaves: UIImageView!
    @IBOutlet weak var progressViewBg: UIView!
    @IBOutlet weak var progressDescriptionLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
   // @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var backView: BTabView!
    
    @IBOutlet weak var imgBonus: UIImageView!

   // @IBOutlet weak var remainingTimeView: UIView!
    
  //  @IBOutlet weak var remainingTimeLabel: UILabel!
    fileprivate var collectionViewLayout: LGHorizontalLinearFlowLayout!
    
    fileprivate var animationsCount = 0
    
    var campaign: Campaign!
    var campaignColor = UIColor(hexString: "#ffffff")
    
    var loader: NVActivityIndicatorView!
    
    fileprivate var pageWidth: CGFloat {
        return self.collectionViewLayout.itemSize.width + self.collectionViewLayout.minimumLineSpacing
    }
    
    fileprivate var contentOffset: CGFloat {
        return self.collectionView.contentOffset.x + self.collectionView.contentInset.left
    }

    var missions: [Mission] = [Mission]()
    var clickedMission: Mission?
    
    var timer: Timer?
    
    static let MissionsToMissionShow = "MissionsToMissionShowSegue"
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.loader = UIUtils.addLoader(self.view, y: Int(UIScreen.main.bounds.midY))
        self.loader.startAnimating()
        
        self.backView.backgroundColor = campaign.Color
        self.view.backgroundColor = campaign.Color
        self.collectionView.backgroundColor = campaign.Color
        let campaignImageUrl = self.campaign.imageUrl!
        self.campaignImage.kf.setImage(with: URL(string: campaignImageUrl)!)
        self.campaignName.text = self.campaign.name?.uppercased()
        self.progressViewBg.layer.cornerRadius = 20
        self.progressViewBg.clipsToBounds = true
        self.progressLabel.text = "\(campaign.completedMissions!)/\(campaign.totalMissions!)"
        self.setProgressbar(campaign: campaign)
        
        if self.campaign.HierarchyType == .bonus {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(MissionsViewController.reloadRemainingTime), userInfo: nil, repeats: true)
            
        } else {
            self.imgBonus.isHidden = true
            self.progressDescriptionLabel.text = "\(campaign.completedMissions!) DE \(campaign.totalMissions!) MISIONES COMPLETADAS"
        }
    
        
         self.campaignColor = campaign.Color
         imgWaves.image = imgWaves.image?.withRenderingMode(.alwaysTemplate)
         imgWaves.tintColor = campaign.Color.darker(by: 13)

         self.navBarView.backgroundColor = campaign.Color.darker(by: 13)
        
       // let wavy = WavyView(frame: CGRect(x: 0, y: self.imgViewBg.frame.origin.y + self.imgViewBg.frame.height, width: self.imgViewBg.frame.width, height: 50))
      //  imgViewBg.addSubview(wavy)
        //self.imgWaves.setImageColor(color: campaign.Color.darker(by: 13)!)
        
        //Iphone 6s width   375.0
        //iphone 6s height  667.0
        //Iphone 6s cell  CGSizeMake(260, 370)  // width, height
        
        let width   = self.view.frame.width
        let height  = self.view.frame.height
        
        let cellWidth = (260.0/375)*width
        let cellHeight = (370.0/667)*height
        
        self.collectionViewLayout = LGHorizontalLinearFlowLayout.configureLayout(collectionView: self.collectionView, itemSize: CGSize(width: cellWidth, height: cellHeight), minimumLineSpacing: 1)
        self.collectionView.isUserInteractionEnabled = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if let t = timer{
            t.invalidate()
        }
    }
    
    @objc func reloadRemainingTime(){
        self.imgBonus.isHidden = false
        self.progressDescriptionLabel.text = "VENCE EN \(campaign.RemainingTimeMessage)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UIDevice().userInterfaceIdiom == .phone {
            navBarViewHeightConstraint.constant = UIUtils.getHeaderImageHeightForCurrentDevice()
        }
        
//        if let tb = self.tabBarController as? TabBarController{
//            tb.setNavigationTitle("")
//            tb.setNavbarColor(campaign.Color)
//            tb.enableBackButton(true, navController: self.navigationController)
//        }
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       // self.pageControl.numberOfPages = missions.count
        return missions.count
        
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
    
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "MissionCell", for: indexPath) as! BMissionCell
        
        let index: Int = (indexPath as NSIndexPath).row
        let mission = missions[index]
        
        item.Mission = mission
        
        item.backgroundColor = UIColor.clear
        
        
        if self.campaign.HierarchyType == .bonus {
            item.missionCount.text = "BONUS"
        }else{
            item.missionCount.text = "\(index + 1) DE \(missions.count)"
        }
       
        item.openAnsViewBtn.setTitleColor(self.campaignColor, for: UIControl.State.normal)
        
        if (mission.State == (Mission.MissionState.locked)) || (mission.State == (Mission.MissionState.expired)){
            item.openMissionButton.backgroundColor = UIColor.init(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1)   //UIColor(hexString: "969696")?.darker(by: 13)
            item.stateBackgroundView.backgroundColor = UIColor.init(red: 225/255.0, green: 225/255.0, blue: 225/255.0, alpha: 1)
            item.openMissionButton.setTitleColor(UIColor.white, for: .normal)
            item.missionName.textColor = UIColor.init(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1)
            item.missionCount.textColor = UIColor.init(red: 150/255.0, green: 150/255.0, blue: 150/255.0, alpha: 1)
            item.openAnsViewBtn.isHidden = true
            
            
            if let currentCGImage = item.coinsAndExpContainer.image!.cgImage
            {
                let currentCIImage = CIImage(cgImage: currentCGImage)
                
                let filter = CIFilter(name: "CIColorMonochrome")
                filter?.setValue(currentCIImage, forKey: "inputImage")
                
                // set a gray value for the tint color
                filter?.setValue(CIColor(red: 0.7, green: 0.7, blue: 0.7), forKey: "inputColor")
                
                filter?.setValue(1.0, forKey: "inputIntensity")
                let outputImage = filter?.outputImage
                let context = CIContext()
                if let cgimg = context.createCGImage(outputImage!, from: outputImage!.extent) {
                    let processedImage = UIImage(cgImage: cgimg)
                    item.coinsAndExpContainer.image = processedImage
                }
            }
            
            
        } else {
            item.missionName.textColor = self.campaignColor
            item.missionCount.textColor = self.campaignColor
            item.openMissionButton.backgroundColor = self.campaignColor?.darker(by: 13)
            item.openAnsViewBtn.isHidden = false
            item.stateBackgroundView.backgroundColor = UIColor.white
            item.coinsAndExpContainer.image = UIImage(named: "credits_and_exp")
            
        }
        
        item.GoToVerRegistroTapAction = {
            
            let index: Int = (indexPath as NSIndexPath).row
            let mission = self.missions[index]
            
            self.clickedMission = mission
            
            let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AnswersViewController") as! AnswersViewController
            viewController.mission = self.clickedMission
            //viewController.campaignColor = self.campaign?.Color
            viewController.modalPresentationStyle = .overCurrentContext
            self.present(viewController, animated: true, completion: nil)
        }
        
        item.addGoToMissionTapAction = {
            if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
                return
            }
            
            let selectedPage = (indexPath as NSIndexPath).row
            
           // if selectedPage == self.pageControl.currentPage {
                let index: Int = (indexPath as NSIndexPath).row
                let mission = self.missions[index]
                
                self.clickedMission = mission
                
                if mission.State == (Mission.MissionState.expired) {
                    UIUtils.toast("Esta misión no está disponible")
                    return
                }
                
                if mission.State != (Mission.MissionState.locked){
                    self.performSegue(withIdentifier: MissionsViewController.MissionsToMissionShow, sender: self)
                    return
                }
                
                if let dependants = mission.dependants , dependants.count > 0 {
                    let requiredMission = dependants.first!
                    if let name = requiredMission.name{
                        UIUtils.toast("Debes completar la misión \"\(name)\"")
                    }
                }
           // }
            else {
                self.scrollToPage(selectedPage, animated: true)
            }
        }
        return item
    }
    
//    @IBAction func pageControlValueChanged(_ sender: AnyObject) {
//        self.scrollToPage(self.pageControl.currentPage, animated: true)
//    }
    
    fileprivate func scrollToPage(_ page: Int, animated: Bool) {
        self.animationsCount += 1
        let pageOffset = CGFloat(page) * self.pageWidth - self.collectionView.contentInset.left
        self.collectionView.setContentOffset(CGPoint(x: pageOffset, y: 0), animated: true)
        //self.pageControl.currentPage = page
        
    }
    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
        if collectionView.isDragging || collectionView.isDecelerating || collectionView.isTracking {
            return
        }
//        let item = collectionView.cellForItem(at: indexPath) as! BMissionCell
//        item.backgroundColor = BColor.LockedCellBackground
    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        
//        let item = collectionView.cellForItem(at: indexPath) as! BMissionCell
//        item.backgroundColor = BColor.UnlockedCellBackground
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //self.pageControl.currentPage = Int(self.contentOffset / self.pageWidth)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.animationsCount -= 1
        if self.animationsCount == 0 {
            self.collectionView.isUserInteractionEnabled = true
        }
    }
    
    func loadData(){
        CampaignsAPI.getCampaign(campaignId: self.campaign.id!) { (campaign: Campaign?, error: NSError?) in
            
            if let c = campaign {
                
                if let serverMissions = c.missions{
                    self.missions = serverMissions
                    self.collectionView.reloadData()
                    if let cm = self.clickedMission , cm.available(){
                        return
                    }else{
                        self.scrollToFirstAvailableMission()
                    }
                }
                self.loader.stopAnimating()
            }
        }
    }
    
//    @IBAction func backButtonPressed(_ sender: AnyObject) {
//        self.performSegue(withIdentifier: "CampaignShowToCampaignsSegue", sender: sender)
//    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if self.clickedMission != nil{
            return true
        }else{
            return false
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            if identifier == MissionsViewController.MissionsToMissionShow{
                let controller = segue.destination as! MissionViewController
                controller.mission = self.clickedMission!
                controller.campaign = self.campaign
            }
        }
    }
    
    
    
 
    fileprivate func scrollToFirstAvailableMission(){
        
        if missions.count == 0{
            return
        }
        
        for i in 0...missions.count-1{
            if missions[i].available(){
                self.scrollToPage(i,animated: true)
                return
            }
        }
    }
    
    func setProgressbar(campaign: Campaign) {
        self.progressBar.progress = Float(campaign.completedMissions!) / Float(campaign.totalMissions!)
        self.progressBar.progressTintColor = campaign.Color
        self.progressBar.layer.cornerRadius = 4
        self.progressBar.layer.masksToBounds = true
    }

    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }

}
