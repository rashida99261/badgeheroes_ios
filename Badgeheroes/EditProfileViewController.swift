//
//  EditProfileViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import SwiftEventBus

import ALCameraViewController

class EditProfileViewController: BModalViewController {

    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    
    @IBOutlet weak var nameField: BUITextField!
    @IBOutlet weak var emailField: BUITextField!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var titleButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tabView: BTabView!
    
    
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var creditsLabelHeader: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    var titles: [Title] = [Title]()
    var current_title_id: Int = -1
    
    fileprivate var progress: CGFloat = 0
    
    @IBOutlet weak var changePasswordButton: BSecondaryButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        self.reloadLogo()
        
        self.progress = 0
        UIUtils.toCircleImage(cameraButton)
        UIUtils.toCircleImage(userImage)
        setFontAwesome()
        
        
        
        self.cameraButton.backgroundColor = BColor.PrimaryBar
        self.view.backgroundColor = BColor.PrimaryBar
        
        UsersAPI.editUser { (user, error) in
            if let u = user{
                
                self.creditsLabelHeader.text = "\(UIUtils.clean(u.credits))"
                
                if let titles = u.titles , titles.count > 0{
                    self.titles = titles
                    for t in titles{
                        if let active = t.active , active == true{
                            self.current_title_id = t.id!
                            self.titleButton.setTitle(t.name, for: UIControl.State())
                            break
                        }
                    }
                    
                }else{
                    self.titleButton.setTitle("Aún no ganas ningún titulo", for: UIControl.State())
                }
            }
        }
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
        
        
        self.progressBar.isHidden = true
        self.progressBar.setProgress(0, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navBarView.backgroundColor = BColor.PrimaryBar
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if Settings.ImageUrl.count > 0{
            userImage.kf.setImage(with: URL(string: Settings.ImageUrl)!)
            
        }
        
        
        nameField.text = Settings.FullName
        emailField.text = Settings.Email
        
        changePasswordButton.unlockButton()
        
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
        let fullName = nameField.text! as NSString
        let fullNameArr = fullName.components(separatedBy: " ")
        let firstName = fullNameArr[0]
        var lastName = ""
        if(fullNameArr.count > 1){
            lastName = fullNameArr[1]
        }
        let email = emailField.text!
        
        UsersAPI.updateUser(id: Settings.UserId, email: email, firstName: firstName, lastName: lastName, imageUrl: "", titleId: self.current_title_id) { (user: User?, error: NSError?) in
            if let u = user{
                Settings.Email = u.email!
                Settings.FirstName = u.firstName!
                Settings.LastName = u.lastName!
                Settings.ImageUrl = u.imageUrl!
                UIUtils.toast("Se han actualizado los datos")
                if let callback = self.modalFinished{
                    callback()
                }
            }
            
        }
        
        
    }
    
    @IBAction func clickONBackBtn(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cameraButtonPressed(_ sender: AnyObject) {
        let cameraViewController = CameraViewController { [weak self] image, asset in
            if let img = image{
                let compressImg = UIUtils.resizeImage(img)
                let fileName = "user_\(Settings.UserId).jpeg"
                let data = compressImg.jpegData(compressionQuality: 0.5)
                self?.progressBar.isHidden = false
                AmazonAPI.uploadFile(fileName, data: data!, progressCallback: (self?.uploadProgressCallback)! , finishCallback: (self?.uploadFinishedCallback)!)

                self?.dismiss(animated: true, completion: nil)
            }else{
                self?.dismiss(animated: true, completion: nil)
            }
        }
        present(cameraViewController, animated: true, completion: nil)
    }
    
    //(bytesSent, totalBytesSent, totalBytesExpectedToSend)
    func uploadProgressCallback(_ dataSent: Int64?, totalDataSent: Int64?, totalDataExpected: Int64?){
        if let dSent = totalDataSent, let tDSent = totalDataExpected{
            let num = Double(dSent)
            let den = Double(tDSent)
            let progress = CGFloat(num/den)
            if progress > self.progress{
                self.progress = progress
                DispatchQueue.main.async(execute: {
                    self.progressBar.setProgress(Float(progress), animated: true)
                })
            }
        }
    }
    
    func uploadFinishedCallback(_ error: NSError?, exception: String?, nsUrl: URL?){
        if let url = nsUrl{
            UsersAPI.updateUser(url.absoluteString, callback: { (user: User?, error:NSError?) in
                if let u = user{
                    Settings.ImageUrl = u.imageUrl!
                    self.userImage.kf.setImage(with: URL(string: Settings.ImageUrl)!, options: [.forceRefresh])
                    DispatchQueue.main.async(execute: {
                        self.progressBar.isHidden = true
                        self.progressBar.setProgress(0, animated: false)
                    })
                    UIUtils.toast("Foto actualizada")
                }
            })
        }else{
            //UIUtils.toast("Error de conexión. Intenta mas tarde")
        }
    }
    
    @IBAction func titleButtonPressed(_ sender: AnyObject) {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for title in titles{
            let action = UIAlertAction(title: title.name, style: .default, handler: {
                (alert: UIAlertAction!) -> Void in
                
                self.current_title_id = title.id!
                self.titleButton.setTitle(title.name!, for: UIControl.State())
            })
            optionMenu.addAction(action)
        }
        
        let action = UIAlertAction(title: "Cancelar", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(action)
        self.present(optionMenu, animated: true, completion: nil)
        
        
    }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    @IBAction func changePasswordButtonPressed(_ sender: Any) {
        let c = self.storyboard?.instantiateViewController(withIdentifier: "EditPasswordViewController") as! EditPasswordViewController
        UIUtils.showInDefaultModal(self, controller: c)
    }
    
    fileprivate func setFontAwesome(){
        cameraButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 30, style: .regular)
        //cameraButton.setTitle(String.fontAwesomeIcon(name: .camera), for: UIControl.State())
        
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
}
