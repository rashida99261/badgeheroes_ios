//
//  AppDelegate.swift
//  copyapp
//
//  Created by Gonzalo Lopez on 5/16/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import CoreData
import SlideMenuControllerSwift
import AWSCore
import Fabric

import Crashlytics
import Firebase
import FirebaseInstanceID
import AudioToolbox
import Siren
import UserNotifications
import BRYXBanner
//import Rollbar
import IQKeyboardManagerSwift

var GlobalBgColor : UIColor?  //= UIColor(hexString: "#005578")!
var strColl = ""
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var loginDate: Date?
    
    let cognitoId: String = "us-east-1:1a3e9782-65c3-43d3-974e-c13955df1d1d"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        /*let config: RollbarConfiguration = RollbarConfiguration()
        config.environment = "production"
        
        Rollbar.initWithAccessToken("f2a3756c2f4c45ca8aca4b1f0140f973", configuration: config)
        */
        Fabric.with([Crashlytics.self])
        
        IQKeyboardManager.shared.enable = true
        
        BColor.toDefaultColors()
        
        if(Settings.AccessToken == ""){
            Settings.logout()
        }

        SlideMenuOptions.leftViewWidth = 200
        
        loadCustomColors()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let firstViewController: UIViewController
        
        firstViewController = storyboard.instantiateViewController(withIdentifier: "MainViewController")
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor.gray
        pageControl.currentPageIndicatorTintColor = UIColor.blue
        pageControl.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 1.00)
        pageControl.frame = CGRect(x: 0, y: 0, width: pageControl.frame.size.width, height: 10);
        
        
        self.window?.rootViewController = firstViewController
        
        self.window?.makeKeyAndVisible()
        
        //AMAZON
        let credentialsProvider = AWSCognitoCredentialsProvider(
            regionType: AWSRegionType.USEast1,
            identityPoolId: cognitoId)
        
        let configuration = AWSServiceConfiguration(
            region: AWSRegionType.USEast1,
            credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        
        
        let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        guard let googleServicePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist") else { return false }
        
        let firOptions = FirebaseOptions.init(contentsOfFile: googleServicePath)
        FirebaseApp.configure(options: firOptions!)
        
//        if let token = InstanceID.instanceID().token(){
//            print(token)
//        }

        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        
        UsersAPI.updateFirebaseToken({ (user, error2) in
            if error2 == nil{
                if let fat = user?.firebaseAccessToken{
                    Settings.FirebaseAccessToken = fat
                }
                
                if let tid = user?.teamId{
                    Settings.TeamId = tid
                }
                
                if let cid = user?.clientId{
                    Settings.ClientId = cid
                }
                
            }
        })

        Siren.shared.wail()
        Siren.shared.presentationManager = PresentationManager(forceLanguageLocalization: .spanish)
        Siren.shared.rulesManager = .init(globalRules: .init(promptFrequency: .immediately, forAlertType: .option), showAlertAfterCurrentVersionHasBeenReleasedForDays: 1)
        Siren.shared.apiManager = .init(countryCode: "cl")
//        let siren = Siren.sharedInstance
        // Optional: Defaults to .Option
//        siren.alertType = .none
//        siren.countryCode = "cl"
//        siren.delegate = self
//        Siren.sharedInstance.checkVersion(checkType: .immediately)
        
        connectToFcm()
//        FIRMessaging.messaging().delega
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().setAPNSToken(deviceToken, type: Environment.FCM_ENVIRONMENT)
    }
   
    @objc func tokenRefreshNotification(_ notification: Notification) {
//        if let refreshedToken = InstanceID.instanceID().token(){
//            print("InstanceID token: \(refreshedToken)")
//            Settings.FcmToken = refreshedToken
//
//            if Settings.IsLoggedIn{
//                UsersAPI.updateDevice({ (response, error) in
//                    print("Update device")
//                })
//            }
//
//            // Connect to FCM since connection may have failed when attempted before having a token.
//            connectToFcm()
//        }
        
    }
    // [END refresh_token]
    
    // [START connect_to_fcm]
    func connectToFcm() {
        Messaging.messaging().shouldEstablishDirectChannel = true
//        Messaging.messaging().connect { (error) in
//            if let e = error {
//                print("Unable to connect with FCM. \(e)")
//            } else {
//                print("Connected to FCM.")
//            }
//        }
    }
    
    
    //FCM NOTIFICATION RECEIVED
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        if let aps = userInfo["aps"] as? NSDictionary {
//            print(aps)
            if let alert = aps["alert"] as? NSDictionary {
//                print(alert)
                if let title = alert["title"] as? String, let body = alert["body"] as? String{
//                    print(title)
//                    print(body)
                    let banner = Banner(title: title, subtitle: body, image: UIImage(named: "ic_notification"), backgroundColor: BColor.NotificationBackgroundColor)
                    banner.dismissesOnTap = true
                    banner.show(duration: 3.0)
                    
                }
            }
            
        }
        

    }
    
    
    
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if let inDate = loginDate{
            let outDate = Date()
            
            BFirebase.pushSessionFinished(inDate, endDate: outDate)
            
        }
        
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        
    }
    
    
    

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        loginDate = Date()
        //Siren.sharedInstance.checkVersion(checkType: .daily)
        Siren.shared.rulesManager = .init(globalRules: .init(promptFrequency: .daily, forAlertType: .option))
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
        
    }

    // MARK: - Core Data stack

    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.mispistachos.copyapp" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()

    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "badgeheroes", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()

    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    
    func application(_ application: UIApplication, shouldRestoreApplicationState coder: NSCoder) -> Bool {
        return false
    }
    
    func application(_ application: UIApplication, shouldSaveApplicationState coder: NSCoder) -> Bool {
        return false
    }
    
    func moviePlayerWillEnterFullscreenNotification(_ notification: Notification){
     //   self.rotation
    }
    
    func moviePlayerWillExitFullscreenNotification(_ notification: Notification){
        
    }
    
    
    
    
    
    
    
    func loadCustomColors(){
        let semaphore: DispatchSemaphore  = DispatchSemaphore(value: 0)
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            ClientsAPI.getSettings { (bconfig: BConfig?, error: NSError?) in
                if let config = bconfig{
                    
                    if let imagePath = config.x3{
                        Settings.setColors(config)
                        Settings.Logo = imagePath
                        semaphore.signal()
                    }else{
                        semaphore.signal()
                    }
                    
                    
                    
                }else{
                    
                    semaphore.signal()
                    
                    
                }
            }
        }
        
        
        
        
        //Wait for the request to complete
        
        
        while semaphore.wait(timeout: DispatchTime.now()) != .success  {
            RunLoop.current.run(mode: RunLoop.Mode.default, before: Date(timeIntervalSinceNow: 2))
        }
        
        BColor.reloadColors()
    }
    
    
    func sirenDidDetectNewVersionWithoutAlert(_ message: String) {
        
        
        UIUtils.runOnUIThread {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let modalController = storyboard.instantiateViewController(withIdentifier: "RewardPopupController") as! RewardPopupController
            modalController.titleMessage = "¡Hay una nueva versión disponible!\n Puedes obtenerla presionando el botón que se encuentra a continuación"
            modalController.imageUrl = "shadowed_logo"
            modalController.fromLocal = true
            modalController.credits = 0
            modalController.modalPresentationStyle = .overCurrentContext
            
            
            
            if let r = self.window?.rootViewController{
                
                
                modalController.modalCanceled = {
                    r.dismiss(animated: true, completion: nil)
                }
                modalController.modalFinished = {
                    self.launchAppStore()
                    r.dismiss(animated: true, completion: {
                        
                    })
                }
                r.present(modalController, animated: true, completion: {
                    modalController.continueButton.setTitle("ACTUALIZAR", for: UIControl.State())
                })
                
            }
            
        }
        
        
    }
    
    func launchAppStore() {
        
            let iTunesString =  "https://itunes.apple.com/app/id1127412895"
            let iTunesURL = URL(string: iTunesString)
            
            DispatchQueue.main.async {
                UIApplication.shared.openURL(iTunesURL!)
            }
        
       
    }
    

}

