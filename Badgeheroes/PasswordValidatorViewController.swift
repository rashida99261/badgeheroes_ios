//
//  PasswordValidatorViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import Kingfisher

class PasswordValidatorViewController: BModalViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var acceptButton: BSecondaryButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIUtils.toCircleImage(self.userImage)
        
        userEmail.text = Settings.Email
        let imageUrl = Settings.ImageUrl
        userImage.kf.setImage(with: URL(string: imageUrl )!)
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.enableNavbar?(true)
    }
    
}
