//
//  MissionViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/27/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class MissionViewController: BaseUIViewController {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var missionCount: UILabel!
    @IBOutlet weak var missionName: UILabel!
    @IBOutlet weak var missionImage: UIImageView!
    @IBOutlet weak var missionDescription: UILabel!
    @IBOutlet weak var missionExperience: UILabel!
    @IBOutlet weak var missionCredits: UILabel!
    
    @IBOutlet weak var missionDescTV: UITextView!
    @IBOutlet weak var descTVHt : NSLayoutConstraint!
    
   // @IBOutlet weak var navBarView: UIView!
    //@IBOutlet weak var navBarViewHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var imgWaves: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var containerView: BTabView!
    @IBOutlet weak var longAcceptButton: BPrimaryButton!
    
    
    //@IBOutlet weak var tagsView: UIView!
    
    
    @IBOutlet weak var totalRepeatsProgressBar: UIProgressView!
    
    //@IBOutlet weak var watchAnswersLabel: UILabel!
    
    @IBOutlet weak var totalRepeatsLabel: UILabel!
    @IBOutlet weak var totalRepeatsMessage: UILabel!
    
    @IBOutlet weak var totalRepeatsProgressBarHeight: NSLayoutConstraint!
    
    @IBOutlet weak var repeatsView: UIView!
    
    @IBOutlet weak var acceptedRepeats: UILabel!
    @IBOutlet weak var waitingRepeats: UILabel!
    @IBOutlet weak var rejectedRepeats: UILabel!
    
    @IBOutlet weak var repeatsLabel: UILabel!
    
    var mission: Mission?
    var campaign: Campaign?
    var userMission: Bool = true
    
    static let MissionToMissionPagesSegue = "MissionToMissionPagesSegue"
    static let MissionToMissionAnswersSegue = "MissionToMissionAnswersSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.repeatsLabel.text = ""
        
//        self.loader.startAnimating()
        
        //self.navBarView.backgroundColor = campaign?.Color.darker(by: 13)
       // self.darkColorView.backgroundColor = campaign?.Color.darker(by: 13)
        
        imgWaves.image = imgWaves.image?.withRenderingMode(.alwaysTemplate)
        imgWaves.tintColor = campaign?.Color.darker(by: 13)
        
        self.totalRepeatsProgressBar.progressTintColor = campaign?.Color
        self.totalRepeatsProgressBar.transform=CGAffineTransform(scaleX: 1.0, y: 2.0)
        self.totalRepeatsProgressBar.layer.masksToBounds = true
        self.totalRepeatsProgressBar.clipsToBounds = true
        self.totalRepeatsProgressBar.layer.cornerRadius = 4
        //self.watchAnswersLabel.isHidden = true
        
        let callback = {
            (mission: Mission?, error: NSError?) -> Void in
            if let mm = mission {
                self.mission = mm
                if mm.id != nil {
                    
                    if let dailyDoneRepeats = mm.dailyDoneRepeats, let dailyMaxRepeats = mm.dailyMaxRepeats{
                        if dailyMaxRepeats >= 1{
                            self.showDailyRepeatsInfo(done: dailyDoneRepeats, max: dailyMaxRepeats)
                        }
                    }
                    
                    if let totalDoneRepeats = mm.totalDoneRepeats, let totalMaxRepeats = mm.totalMaxRepeats{
                        
                        if totalDoneRepeats > 0 {
                            //self.watchAnswersLabel.isHidden = false
                            //self.watchAnswersLabel.textColor = UIColor(hexString: "FFFFFF")
                        }
                        
                        if totalMaxRepeats > 1{
                            
                            self.showTotalRepeatsInfo()
                            let prog = Float(totalDoneRepeats)/Float(totalMaxRepeats)
                            self.totalRepeatsProgressBar.progress = prog
                            self.totalRepeatsLabel.text = "\(totalDoneRepeats)/\(totalMaxRepeats)"
                        }else{
//                            self.totalRepeatsMessageHeightConstraint.constant = 0
//                            self.totalRepeatsProgressBarHeight.constant = 0
//                            self.totalRepeatsLabelHeight.constant = 0
                            
                            let repeatViews: [UIView] = [self.totalRepeatsLabel, self.totalRepeatsMessage, self.totalRepeatsProgressBar]
                            
                            for rv in repeatViews{
                                let heightConstraint = NSLayoutConstraint(item: rv, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 0)
                                self.view.addConstraint(heightConstraint)
                            }
                            self.view.layoutIfNeeded()
                        }
                    }
                    
                    if let finishedAnswers = mm.finishedAnswers,
                        let acceptedAnswers = mm.acceptedAnswers,
                        let rejectedAnswers = mm.rejectedAnswers{
                        
                        self.waitingRepeats.text = String(finishedAnswers)
                        self.acceptedRepeats.text = String(acceptedAnswers)
                        self.rejectedRepeats.text = String(rejectedAnswers)
                    }
                    
                    UIView.animate(withDuration: 5, animations: {
                        self.missionName.text = UIUtils.clean(mm.name)
                        self.missionCount.text = "\(1) DE \(1)"
                        self.missionImage.kf.setImage(with: (URL(string: mm.imageUrl!)!))
                        self.missionExperience.text = "\(UIUtils.clean(mm.experience))"
                        self.missionCredits.text = "\(UIUtils.clean(mm.credits))"
//                        self.missionDescription.text = UIUtils.clean(mm.description)
                        self.missionDescTV.text = UIUtils.clean(mm.description)
                        //self.descTVHt.constant =  UIUtils.clean(mm.description).height(withConstrainedWidth: self.missionDescTV.frame.width, font: UIFont.init(name: "TitilliumWeb-Semibold", size: 14)!) + 10
                    })
                    
                    self.setTagsView(mm)
                    
                    switch(mm.State){
                    case .accepted:
                        self.toAcceptedMission()
                    case .finished:
                        self.toFinishedMission()
                    case .noStarted:
                        self.toNoStartedMission()
                    case .started:
                        self.toStartedMission()
                    case .dailyLimit:
                        self.toMaxRepeatsLimit()
                    default:
                        self.toNotAvailableMission()
                        print("estado de la mision no encontrado")
                    }
                    self.loader.stopAnimating()
                }
            }
        }
        
        if let m = self.mission{
            
            if userMission == true{
                MissionsAPI.getMission(missionId: m.id!, callback: callback)
            }else{
                MissionsAPI.getInfo(missionId: m.id!, callback: callback)
            }
            
        }
    }
    
    func setTagsView(_ mission: Mission){
        let screenTagViewY = UIScreen.main.bounds.height - 50
        
        var defaultTagViewY = self.view.convert(CGPoint(x: 0,y: screenTagViewY), from: nil).y
        
        if self.longAcceptButton.frame.maxY > defaultTagViewY{
            defaultTagViewY = self.longAcceptButton.frame.maxY + 10
        }
        
     //   self.tagsView.frame = CGRect(x: self.longAcceptButton.frame.minX, y: defaultTagViewY, width: self.tagsView.frame.width, height: self.tagsView.frame.height)
        
        var lastTagX = CGFloat(0)
        for t in mission.Tags{
            
            if t.count > 0{
                let label = UILabel()
                label.text = "#\(t)"
                label.font = UIFont.boldSystemFont(ofSize: 14)
                
                label.sizeToFit()
                let frame = CGRect(x: lastTagX, y: 0, width: label.frame.width, height: label.frame.height)
                label.frame = frame
                lastTagX = lastTagX + label.frame.width + 10
                print(label.frame)
                //descomentar para agregar tags
                //self.tagsView.addSubview(label)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.longAcceptButton.unlockButton()
        
       // if UIDevice().userInterfaceIdiom == .phone {
          //  navBarViewHeightConstraint.constant = UIUtils.getHeaderImageHeightForCurrentDevice()
       // }
    
        if let _ = self.tabBarController as? TabBarController{
            self.view.backgroundColor = campaign?.Color
            self.scrollView.backgroundColor = campaign?.Color
            self.containerView.backgroundColor = campaign?.Color
            self.mainView.backgroundColor = UIColor(hexString: "FFFFFF")
            self.repeatsView.backgroundColor = UIColor(hexString: "FFFFFF")
            self.missionName.textColor = campaign?.Color
            self.longAcceptButton.backgroundColor = campaign?.Color.darker(by: 13)
            self.longAcceptButton.setTitleColor(.white, for: .normal)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
        acceptMission()
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if self.mission != nil{
            return true
        }else{
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == MissionViewController.MissionToMissionPagesSegue{
            //let viewController = segue.destinationViewController as! ContainerPagesViewController
            let viewController = segue.destination as! BPagesViewController
            GlobalBgColor = (campaign?.Color)!
            viewController.creditPass = Int(self.missionCredits.text!)
            viewController.expPass = Int(self.missionExperience.text!)
            viewController.mission = self.mission
        }
        
        if segue.identifier == MissionViewController.MissionToMissionAnswersSegue{
            //let viewController = segue.destinationViewController as! ContainerPagesViewController
           // let viewController = segue.destination as! AnswersViewController
            //viewController.mission = self.mission
            //viewController.campaignColor = self.campaign?.Color
        }
    }
    
    @IBAction func repeatsViewPressed(_ sender: Any) {
        self.performSegue(withIdentifier: MissionViewController.MissionToMissionAnswersSegue, sender: self)
    }
    
    @IBAction func watchAnswersButtonPressed(_ sender: Any) {
         self.performSegue(withIdentifier: MissionViewController.MissionToMissionAnswersSegue, sender: self)
    }
    
    func acceptMission() {
        if let m = self.mission{
            if m.available(){
                MissionsAPI.acceptMission(missionId: m.id!, callback: { (mission: Mission?, error: NSError?) in
                    
                    if let mm = mission{
                        if mm.id != nil{
                            self.mission = mm
                            self.performSegue(withIdentifier: MissionViewController.MissionToMissionPagesSegue, sender: self)
                            return
                    
                        }else{
                            UIUtils.toast("Problema de conexión")
                        }
                    }
                })
            }
        }
    }
    
    func toAcceptedMission(){
        self.longAcceptButton.isHidden = false
        self.longAcceptButton.setTitle("MISIÓN FINALIZADA", for: UIControl.State())
        self.longAcceptButton.isEnabled = false
    }
    
    func toFinishedMission(){
        self.longAcceptButton.isHidden = false
        self.longAcceptButton.setTitle("ESPERANDO REVISIÓN", for: UIControl.State())
        self.longAcceptButton.isEnabled = false
    }
    
    func toNoStartedMission(){
        self.longAcceptButton.isHidden = false
        self.longAcceptButton.setTitle("COMENZAR", for: UIControl.State())
        self.longAcceptButton.isEnabled = true
        
    }
    
    func toStartedMission(){
        self.longAcceptButton.isHidden = false
        self.longAcceptButton.setTitle("COMENZAR", for: UIControl.State())
        self.longAcceptButton.isEnabled = true
    }
    
    func toNotAvailableMission(){
        self.longAcceptButton.isHidden = true
        self.longAcceptButton.setTitle("", for: UIControl.State())
        self.longAcceptButton.isEnabled = false
    }
    
    func toMaxRepeatsLimit(){
        self.longAcceptButton.isHidden = false
        self.longAcceptButton.setTitle("RESPONDIDA POR HOY", for: UIControl.State())
        self.longAcceptButton.lockButton()
    }
    
    func showTotalRepeatsInfo(){
        
        self.totalRepeatsProgressBar.isHidden = false
        self.totalRepeatsLabel.isHidden = false
        self.totalRepeatsMessage.isHidden = false
    }
    
    
    func showDailyRepeatsInfo(done: Int, max: Int){
        self.repeatsLabel.isHidden = false
        let remaining = max - done
        
        if let m = mission, m.Repeatable{
            if remaining == 1{
                self.repeatsLabel.text = "Nota: Puedes responder 1 vez más por hoy"
            }else if remaining > 1{
                self.repeatsLabel.text = "Nota: Puedes responder \(remaining) veces más por hoy"
            }else{
                self.repeatsLabel.text = ""
            }
        }else{
            self.repeatsLabel.text = ""
        }
    }
    
    func hideTotalRepeatsInfo(){
        self.totalRepeatsProgressBar.isHidden = true
        self.totalRepeatsLabel.isHidden = true
        self.totalRepeatsMessage.isHidden = true
    }
    
    func hideDailyRepeatsInfo() {
        self.repeatsLabel.isHidden = true
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}


extension String
{
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
}
