//
//  BUILabel.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/2/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BUILabel: UILabel {

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.sizeToFit()
    }
}

class BUILightLabel: UILabel {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textColor = BColor.Light
    }
}


class BUIDarkLabel: UILabel {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textColor = BColor.Dark
    }
}


class BUIButtonLabel: UILabel {
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textColor = BColor.ButtonNormal
    }
}
