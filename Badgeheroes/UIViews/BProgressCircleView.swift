//
//  BProgressCircleView.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 10/14/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation

public class BProgressCircleView: KDCircularProgress {
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.progressThickness = 0.5
        
        self.trackThickness = 0.6
        self.clockwise = true
        self.trackColor = BColor.TabBackground
        self.set(colors: BColor.Green)
        
    }

    public func setMissionProgress(doneRepeats: Int, totalRepeats: Int){
        self.startAngle = -90
        var finishAngle = self.startAngle
        if(totalRepeats != 0){
            finishAngle = Double(doneRepeats*360)/Double(totalRepeats)
        }
        
        
        self.animate(toAngle: Double(finishAngle), duration: 0.00001) { (boolean) in
            
        }
        
        
    }
}
