//
//  BListSelectionRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/29/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BListSelectionRow: BBaseRow{
    
    var buttonPressed: (() -> Void)?
    
    @IBOutlet weak var selectionButton: UIButton!
   
    @IBOutlet weak var iconButton: UILabel!
    
    @IBAction func selectionButtonPressed(_ sender: AnyObject) {
        if let bp = buttonPressed{
            bp()
        }
    }
    
    func getSelectOptionDialog() -> UIAlertController{
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        
        if let peos = self.pageElement.pageElementOptions{
            for peo in peos{
                let action = UIAlertAction(title: peo.optionValue, style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    for peo in peos{
                        peo.answerSelected = false
                    }
                    
                    peo.answerSelected = true
                    self.selectionButton.setTitle(peo.optionValue, for: UIControl.State())
                })
                optionMenu.addAction(action)
            }
            
            let action = UIAlertAction(title: "Cancelar", style: .cancel, handler: {
                (alert: UIAlertAction!) -> Void in
                
                
            })
            optionMenu.addAction(action)
        }
        return optionMenu
    }
    
    override func completePage() {
        if let peos = self.pageElement.pageElementOptions{
            for peo in peos{
                if peo.answerSelected == true{
                    self.pageElement.isAnswered = true
                    
                }
            }
        }
        
    }
    
    override func loadDefault(){
        for peo in pageElement.pageElementOptions!{
            if let ansSelected = peo.answerSelected , ansSelected == true{
                self.selectionButton.setTitle(peo.optionValue, for: UIControl.State())
                return
            }
        }
    }
    
}
