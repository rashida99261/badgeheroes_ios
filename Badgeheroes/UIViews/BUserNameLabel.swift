//
//  BUserNameLabel.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/4/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BUserNameLabel: UILabel {
    
    fileprivate var user: User?
    fileprivate var delegateController: UIViewController?
    var modal: Bool = true
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        
    }
    
    
    func initLabel(_ user: User, delegateController: UIViewController){
        self.user = user
        self.delegateController = delegateController
        let userName = user.FullName
        self.text = userName
        self.isUserInteractionEnabled = true
        let userTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.userNamePressed(_:)))
        self.addGestureRecognizer(userTap)
        
        
    }
    
    
    
    @objc func userNamePressed(_ sender: UITapGestureRecognizer){
        
        if let dc = delegateController{
            if let u = self.user{
                
                let controller = dc.storyboard?.instantiateViewController(withIdentifier: "HeroViewController") as! HeroViewController
                
                
                controller.userId = u.id!
                
                controller.hideBackButton = false
                
                if self.modal{
                    UIUtils.showInDefaultModal(dc, controller: controller)
                }else{
                    
                    controller.hidesBottomBarWhenPushed = true
                    dc.navigationController?.pushViewController(controller, animated: true)
                    
                }
                
                
            }

        }
        
    }
    
    
}
