//
//  BLegendCell.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BActivityCell: UITableViewCell {

   // @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var logImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userMessage: UILabel!
    
    
}

class BActivityCellGeneral: UITableViewCell {
    
    //@IBOutlet weak var userName: UILabel!
    @IBOutlet weak var logImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var userMessage: UILabel!
    
    
}
