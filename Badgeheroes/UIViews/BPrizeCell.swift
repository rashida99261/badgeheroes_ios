//
//  File.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BPrizeCell: UICollectionViewCell {

    @IBOutlet weak var credits: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var prizeName: UILabel!
    
   // @IBOutlet weak var creditsIcon: UILabel!
   // @IBOutlet weak var leftView: UIView!
    
    func setColors(){
//        leftView.backgroundColor = BColor.SecondaryBar
//        credits.backgroundColor = BColor.SecondaryBar
//        creditsIcon.backgroundColor = BColor.SecondaryBar
        //self.backgroundColor = UIColor.clear
    }
    
    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//        setNeedsLayout()
//        layoutIfNeeded()
//        let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//        var frame = layoutAttributes.frame
//        frame.size.height = ceil(size.height)
//        layoutAttributes.frame = frame
//        return layoutAttributes
//    }
}


class tblPrizeCell: UITableViewCell {
    
    @IBOutlet weak var tbl_credits: UILabel!
    @IBOutlet weak var tbl_imageView: UIImageView!
    @IBOutlet weak var tbl_prizeName: UILabel!
    
    
}
