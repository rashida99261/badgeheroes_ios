//
//  BPrizeRowCell.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/19/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BPrizeRowCell: UITableViewCell {
    
    @IBOutlet weak var prizeName: UILabel!
    @IBOutlet weak var prizeImage: UIImageView!
    @IBOutlet weak var prizeState: UILabel!
    @IBOutlet weak var prizeCredits: UILabel!
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var viewVredit: UIView!
    
    
    func toWaiting(){
        self.prizeState.text = "En espera"
        //self.prizeState.textColor = BColor.Dark
    }
    
    func toAccepted(){
        self.prizeState.text = "Aceptado"
        self.prizeState.textColor = BColor.Green
    }
    
    func toRejected(){
        self.prizeState.text = "Rechazado"
        self.prizeState.textColor = BColor.Red
    }
}
