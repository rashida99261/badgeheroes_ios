//
//  BNavigationBar.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/7/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BNavigationBar: UINavigationBar {

    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.barTintColor = BColor.PrimaryBar
        self.tintColor = BColor.TextButtonNormal
        
        self.titleTextAttributes = [NSAttributedString.Key.foregroundColor : BColor.TextButtonNormal]
    }
    
}
