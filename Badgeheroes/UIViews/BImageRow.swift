//
//  BImageRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/30/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BImageRow: BBaseRow{
    
    @IBOutlet weak var imageButton: UIImageView!
    var imagePressed: ((UIImageView) -> Void)?
    
    @IBAction func imageButtonPressed(_ sender: AnyObject) {
        if let ip = imagePressed{
            ip(self.imageButton)
        }
    }
    
    override func completePage() {
    }
}
