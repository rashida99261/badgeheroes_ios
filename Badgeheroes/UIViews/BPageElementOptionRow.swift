//
//  BPageElementOptionRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/28/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BPageElementOptionRow: BBaseRow {
    
    @IBOutlet weak var optionButton: BOptionButton!
    
    @IBOutlet weak var blabel: UILabel!
    @IBOutlet weak var blabelVw: UIView!
    @IBOutlet weak var blabelLeading: NSLayoutConstraint!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var checkedImage  : UIImage!
    var uncheckedImage: UIImage!
    
    var pageElementOption: PageElementOption!
    let correctImage = UIImage(named: "fa-check-circle_25")!
    let wrongImage = UIImage(named: "fa-times-circle_25")!
    let missingImage = UIImage(named: "fa-circle-o_25")!
    var watchAnswer: Bool!
    
    func initOptionRow(checkedImage: UIImage, uncheckedImage: UIImage, watchAnswer: Bool){
        self.checkedImage = checkedImage
        self.uncheckedImage = uncheckedImage
        self.optionButton.optionRow = self
        self.optionButton.setImage(self.uncheckedImage, for: UIControl.State())
        self.watchAnswer = watchAnswer
    }
    
    func check(){
        pageElementOption.answerSelected = true
//        self.optionButton.setImage(self.checkedImage, for: .normal)
//        self.blabel.backgroundColor = UIColor.init(hexString: "10253F")
//        self.blabel.textColor = UIColor.white
    }
    
    func uncheck(){
        pageElementOption.answerSelected = false
//        self.optionButton.setImage(self.uncheckedImage, for: .normal)
//        self.blabel.backgroundColor = UIColor.init(hexString: "E8E2E6")
//        self.blabel.textColor = UIColor.black
    }
    
    func toggle(){
        if let selected = pageElementOption.answerSelected , selected == true {
            uncheck()
        }else{
            check()
        }
    }
    
    func setColor(color: UIColor){
        self.optionButton.tintColor = color
        self.blabel.textColor = color
    }
    
    override func completePage() {
    }
    
    override func loadDefault() {
        
        if(self.watchAnswer){
            if pageElementOption.IsCorrectAnswer && pageElementOption.AnswerSelected {
                setColor(color: BColor.SuccessGreen)
                self.optionButton.setImage(self.correctImage, for: UIControl.State())
            }else if pageElementOption.IsCorrectAnswer && !pageElementOption.AnswerSelected {
                setColor(color: BColor.SuccessGreen)
                self.optionButton.setImage(self.missingImage, for: UIControl.State())
            }else if !pageElementOption.IsCorrectAnswer && pageElementOption.AnswerSelected{
                setColor(color: BColor.WrongRed)
                self.optionButton.setImage(self.wrongImage, for: UIControl.State())
            }else{
                setColor(color: BColor.MissingGray)
                self.optionButton.setImage(self.missingImage, for: UIControl.State())
            }
        }else{
            if let ansSelected = pageElementOption.answerSelected , ansSelected == true{
                self.check()
            }
        }
    }
}


class BOptionButton: UIButton{
    
    var optionRow: BPageElementOptionRow!
}
