//
//  BLogImageView.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/4/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//


import UIKit

class BItemImageView: UIImageView {
    
    var delegateController: UIViewController!
    var medal: Medal?
    var mission: Mission?
    var title: Title?
    var log: HistoryLog?
    var modal: Bool = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       
        
        
    }
    
    
    func initImage(_ log: HistoryLog, delegateController: UIViewController){
        self.medal = log.medal
        self.mission = log.mission
        self.title = log.title
        self.log = log
        self.delegateController = delegateController
        self.isUserInteractionEnabled = true
        let imageTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imagePressed(_:)))
        self.addGestureRecognizer(imageTap)
        if let _ = URL(string: log.ImageUrl)
        {
            self.kf.setImage(with: URL(string: log.ImageUrl)!)
        }
    }
    
    func initImage(_ mission: Mission, delegateController: UIViewController){
        
        self.mission = mission
        
        self.delegateController = delegateController
        self.isUserInteractionEnabled = true
        let imageTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imagePressed(_:)))
        self.addGestureRecognizer(imageTap)
        if let iu = mission.imageUrl{
            self.kf.setImage(with: URL(string: iu)!)
        }
        
    }
    
    func initImage(_ medal: Medal, delegateController: UIViewController){
        
        self.medal = medal
        
        self.delegateController = delegateController
        self.isUserInteractionEnabled = true
        let imageTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imagePressed(_:)))
        self.addGestureRecognizer(imageTap)
        if let iu = medal.imageUrl{
            self.kf.setImage(with: URL(string: iu)!)
        }
        
    }
    
    @objc func imagePressed(_ sender: UITapGestureRecognizer) {
        
        
        if let dc = self.delegateController{
            var controller: UIViewController?
            
            if let mi = mission{
                let c = dc.storyboard?.instantiateViewController(withIdentifier: "MissionViewController") as! MissionViewController
                c.userMission = false
                c.mission = mi
                controller = c
            }else if let me = medal{
                let c = dc.storyboard?.instantiateViewController(withIdentifier: "MedalViewController") as! MedalViewController
                c.medal = me
                
                controller = c
            }else if let _ = title{
            }
            
            if self.modal{
                if let c = controller{
                    UIUtils.showInDefaultModal(dc, controller: c)
                }
                
            }else{
                if let c = controller{
                    c.hidesBottomBarWhenPushed = true
                    dc.navigationController?.pushViewController(c, animated: true)
                }
            }
        }
    }
}
