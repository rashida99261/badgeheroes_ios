//
//  BMissionCell.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//


import UIKit


class BMissionCell: UICollectionViewCell {
    
    //@IBOutlet weak var missionImg: UIImageView!
    @IBOutlet weak var missionCount: UILabel!
    @IBOutlet weak var missionName: UILabel!
    @IBOutlet weak var missionDescription: UILabel!
    @IBOutlet weak var missionExperience: UILabel!
    @IBOutlet weak var missionCoins: UILabel!
    @IBOutlet weak var coinsAndExpContainer: UIImageView!
    
    @IBOutlet weak var stateBackgroundView: UIView!
    @IBOutlet weak var repeatsLabel: UILabel!
    
    @IBOutlet weak var openMissionButton: UIButton!
    @IBOutlet weak var openAnsViewBtn: UIButton!
    @IBOutlet weak var stateIcon: UIImageView!
    @IBOutlet weak var lblRepeatonIcon: UILabel!
    @IBOutlet weak var stateIconHt: NSLayoutConstraint!
    @IBOutlet weak var stateIconWidth: NSLayoutConstraint!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = BColor.UnlockedCellBackground
    }

    private var mission: Mission!
    
    var remainingRepeats: Int!
    
    var Mission: Mission{
        set(mission){
            self.mission = mission
            self.missionCoins.text = "\(UIUtils.clean(mission.credits))"
            if let sd = mission.shortDescription , sd != ""{
                self.missionDescription.text = UIUtils.clean(sd)
            }else{
                self.missionDescription.text = ""
            }
            
            self.missionExperience.text = "\(UIUtils.clean(mission.experience))"
            self.missionName.text = UIUtils.clean(mission.name).uppercased()
            //let imageUrl = mission.imageUrl!
           // self.missionImg.kf.setImage(with: URL(string: imageUrl)!)
            self.openMissionButton.layer.cornerRadius = 25
            self.openMissionButton.clipsToBounds = true
            
            UIUtils.addShadowToCellView(self)
            
            switch mission.State {
                
            case .accepted:
                self.toAccepted()
            case .finished:
                self.toFinished()
            case .noStarted:
                self.toAvailable()
            case .started:
                self.toAvailable()
            case .locked:
                self.toLocked()
            case .expired:
                self.toExpired()
            case .dailyLimit:
                self.toDailyLimit()
            default:
                self.toLocked()
                print("State not found")
            }
            
            self.layer.cornerRadius = 10
            self.layer.masksToBounds = true
        }
        get{
            return mission
        }
    }
    
    @IBAction func goToMission(_ sender: Any) {
        addGoToMissionTapAction?()
    }
    
    @IBAction func clickOnVerBtn(_ sender: UIButton) {
        GoToVerRegistroTapAction?()
    }
    
    var GoToVerRegistroTapAction : (()->())?
    
    var addGoToMissionTapAction : (()->())?
    
    func toFinished(){
        self.stateIcon.isHidden = false
        self.stateIconHt.constant = 48
        self.stateIconWidth.constant = 40
        self.stateIcon.image = UIImage(named: "ic_mission_under_review")
        self.stateIcon.isUserInteractionEnabled = false
        self.lblRepeatonIcon.isHidden = true
        stateBackgroundView.backgroundColor = BColor.UnlockedCellBackground
        hideRepeatsProgress()
        
        self.bringSubviewToFront(stateBackgroundView)
    }
    
    func toLocked(){
        self.stateIcon.isHidden = false
        self.stateIconHt.constant = 48
        self.stateIconWidth.constant = 40
        self.stateIcon.image = UIImage(named: "ic_blocked_mission")
        self.stateIcon.isUserInteractionEnabled = false
        self.lblRepeatonIcon.isHidden = true
        stateBackgroundView.backgroundColor = UIColor(hexString: "E1E1E1");
        self.missionDescription.textColor = UIColor(hexString: "969696")
        //self.coinsAndExpContainer.setImageColor(color: UIColor(hexString: "F8F8F8")!)
        self.missionCoins.textColor = UIColor(hexString: "969696")
        self.missionExperience.textColor = UIColor(hexString: "969696")
        hideRepeatsProgress()
        self.bringSubviewToFront(stateBackgroundView)
    }
    
    func toAvailable(){
        
        self.stateIcon.isHidden = true
        self.lblRepeatonIcon.isHidden = true
        stateBackgroundView.backgroundColor = BColor.UnlockedCellBackground
        showRepeatsProgress()
        self.sendSubviewToBack(stateBackgroundView)
    }
    
    func toAccepted(){
        self.stateIcon.isHidden = false
        self.stateIcon.isUserInteractionEnabled = false
        self.lblRepeatonIcon.isHidden = true
        self.stateIcon.image = UIImage(named: "ic_mission_complete")
        stateBackgroundView.backgroundColor = BColor.UnlockedCellBackground
        hideRepeatsProgress()
        
        self.bringSubviewToFront(stateBackgroundView)
    }
    
    func toExpired(){
//        setIcon(stateLabel, iconName: "")
      //  stateBackgroundView.backgroundColor = BColor.LockedCellBackground
      //  showRepeatsProgress()
        
        self.stateIcon.isHidden = false
        self.stateIconHt.constant = 48
        self.stateIconWidth.constant = 40
        self.stateIcon.image = UIImage(named: "ic_mission_rejected")
        self.stateIcon.isUserInteractionEnabled = false
        self.lblRepeatonIcon.isHidden = true
        stateBackgroundView.backgroundColor = UIColor(hexString: "E1E1E1")
        self.missionDescription.textColor = UIColor(hexString: "969696")
        //self.coinsAndExpContainer.setImageColor(color: UIColor(hexString: "F8F8F8")!)
        self.missionCoins.textColor = UIColor(hexString: "969696")
        self.missionExperience.textColor = UIColor(hexString: "969696")
        hideRepeatsProgress()
        self.bringSubviewToFront(stateBackgroundView)
        
    }
    
    func toDailyLimit(){
//        setIcon(stateLabel, iconName: "")
        self.stateIcon.isHidden = true
        self.lblRepeatonIcon.isHidden = true
        stateBackgroundView.backgroundColor = BColor.UnlockedCellBackground
        showRepeatsProgress()
        self.bringSubviewToFront(stateBackgroundView)
    }
    
    func setMissionProgress(doneRepeats: Int, totalRepeats: Int){
       
        
        remainingRepeats = totalRepeats - doneRepeats
        
        if mission.Repeatable{
            if remainingRepeats == 1 ||  remainingRepeats > 1{
                self.stateIcon.isHidden = false
                self.repeatsLabel.text = ""
                self.stateIconHt.constant = 48
                self.stateIconWidth.constant = 40
                self.stateIcon.image = UIImage(named: "repeated_icon")
                self.lblRepeatonIcon.isHidden = false
                
                let imageAttachment =  NSTextAttachment()
                imageAttachment.image = UIImage(named:"paper_icon")
                //Set bound to reposition
                imageAttachment.bounds = CGRect(x: 0, y: -2, width: 10, height: 10)
                //Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                //Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                //Add image to mutable string
                completeText.append(attachmentString)
                //Add your text to mutable string
                let  textAfterIcon = NSMutableAttributedString(string: " \(remainingRepeats!)")
                completeText.append(textAfterIcon)
                self.lblRepeatonIcon.attributedText = completeText
                
                
                //set the text and style if any.
//                let maximumLabelSize: CGSize = CGSize(width: self.lblRepeatonIcon.frame.width, height: self.lblRepeatonIcon.frame.height)
//                let expectedLabelSize: CGSize = self.lblRepeatonIcon.sizeThatFits(maximumLabelSize)
//                // create a frame that is filled with the UILabel frame data
//                var newFrame: CGRect = self.lblRepeatonIcon.frame
//                newFrame.size.height = expectedLabelSize.height
               // self.lblRepeatonIcon.frame = newFrame
               ///self.stateIconWidth.constant = self.lblRepeatonIcon.frame.width + CGFloat(2.0)
               // self.stateIconHt.constant = self.lblRepeatonIcon.frame.width + CGFloat(2.0)
                
               // print(self.stateIconWidth.constant)
              //  print(self.stateIconHt.constant)
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                self.stateIcon.isUserInteractionEnabled = true
                self.stateIcon.addGestureRecognizer(tapGestureRecognizer)
                
            }else{
                self.repeatsLabel.text = "Respondida por hoy"
                self.stateIcon.isHidden = true
                self.stateIconHt.constant = 25
                self.lblRepeatonIcon.isHidden = true
            }
        }else{
            self.repeatsLabel.text = ""
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let repeatedVc =  story.instantiateViewController(withIdentifier: "repeatedMissionPopUpVC") as! repeatedMissionPopUpVC
        repeatedVc.strRepeatedCounts = "\(remainingRepeats!)"
        repeatedVc.modalPresentationStyle = .overCurrentContext
        UIApplication.shared.keyWindow?.rootViewController!.present(repeatedVc, animated: true, completion: nil)
        
    }
    
    
    fileprivate func showRepeatsProgress(){
        if let dr = mission.dailyDoneRepeats, let mr = mission.dailyMaxRepeats{
            self.repeatsLabel.isHidden = false
            
            self.setMissionProgress(doneRepeats: dr, totalRepeats: mr)
        }
    }
    
    fileprivate func hideRepeatsProgress(){
        self.repeatsLabel.isHidden = true
    }
    
}
