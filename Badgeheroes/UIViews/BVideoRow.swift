//
//  BVideoRow.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/30/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import MediaPlayer

import AVKit
import AVFoundation

class BVideoRow: BBaseRow{
    
    @IBOutlet weak var videoView: UIView!
    var videoFinished: Bool = false
    var controller: UIViewController?
    override func completePage() {
        
        
        
    }
    
    
    func loadVideo(){
        let url = URL(string: self.pageElement!.mediaUrl!)!
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: self.videoView.frame.width, height: self.videoView.frame.height)
        self.videoView.addSubview(playerViewController.view)
        
        if let vc = controller{
            vc.addChild(playerViewController)
        }
        
        
        //player.play()
    }
    
    func movieFinished(_ notification: Notification){
        
        self.videoFinished = false
    }
    
    func movieNaturalSizeAvailable(_ notification: Notification) {
        print("ola")
    }
    
    
    
}
