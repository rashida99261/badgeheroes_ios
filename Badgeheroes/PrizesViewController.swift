//
//  PrizesViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftEventBus


class PrizesViewController: BCollectionIndexViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var loader: NVActivityIndicatorView!
    
    let segueOnCellClickedName = "PrizesToPrizeSegue"
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    var isSelectView = "collection"
     var userId: Int?
    
    //tableview outlet
    @IBOutlet weak var tblListView: UITableView!
    @IBOutlet weak var btnGrid: UIButton!
    @IBOutlet weak var btnList: UIButton!
    
    @IBOutlet weak var txtSearch: UITextField!
    

    override func viewDidLoad() {
        
        self.reloadLogo()
        
        self.objectsCollectionView = self.collectionView
        self.objectTableView = self.tblListView
        self.searchText = self.txtSearch
        
        super.viewDidLoad()
        self.loader = UIUtils.addLoader(self.view,y: nil)
        
//        self.collectionView.isHidden = true
//        self.tblListView.isHidden = true
        
        loader.startAnimating()
        
        // Do any additional setup after loading the view.
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let id = self.userId{
            UsersAPI.getUser(userId: id, { (serverUser: User?, error:  NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            })
        }else{
            UsersAPI.getProfile { (serverUser: User?, error: NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.navBarView.backgroundColor = BColor.PrimaryBar
        GlobalBgColor = BColor.PrimaryBar
       // btnList.isSelected = true
       // btnGrid.isSelected = false
        
        if(isSelectView == "collection"){
            self.collectionView.isHidden = false
            self.tblListView.isHidden = true
            let origImage = UIImage(named: "list_dark")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            btnList.setImage(tintedImage, for: .normal)
            btnList.tintColor = UIColor.lightGray
            btnGrid.setImage(UIImage(named: "grid_dark"), for: UIControl.State.normal)
        }
        else{
            self.collectionView.isHidden = true
            self.tblListView.isHidden = false
            let origImage = UIImage(named: "grid_dark")
            let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
            btnGrid.setImage(tintedImage, for: .normal)
            btnGrid.tintColor = UIColor.lightGray
            btnList.setImage(UIImage(named: "list_dark"), for: UIControl.State.normal)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func fillCell(_ cell: UICollectionViewCell, object: AnyObject) -> UICollectionViewCell {
        let item = cell as! BPrizeCell
        let prize = object as! Prize
        
        item.prizeName.text = prize.name
        
        if let iurl = prize.imageUrl{
            item.imageView.kf.setImage(with: URL(string: iurl )!)
        }
        item.credits.text = "\(prize.credits!)" //UIUtils.toString(prize.credits!)
        //item.credits.sizeToFit()
        //item.setColors()
        /*if let cid = campaign.campaignCategoryId{
         item.categoryId = cid
         }else{
         item.categoryId = -2
         }*/
        
       // item.backgroundColor = UIColor.white //campaign.Color
       // item.frame = CGRect(x:item.frame.origin.x, y:item.frame.origin.y, width:(self.collectionView.frame.size.width/CGFloat(3)) - 5, height: heights[indexPath.row])
        return item
    }
    
    override func getWidth(index: Int) -> CGFloat {
        return (self.collectionView.frame.size.width/CGFloat(3)) - 5
    }
    
    override func getHeight(index: Int) -> CGFloat {
        return 235
    }
    
   
    override func objectCellName(object: AnyObject) -> String {
        return "PrizeCell"
    }
    override func loadData(){
        PrizesAPI.getPrizes(userId: Settings.UserId) { (prizesResponse: PrizesResponse?, error: NSError?) in
            
            if let serverPrizes = prizesResponse?.prizes{
                self.totalObjects = serverPrizes
                self.objects = self.totalObjects
                self.loader.stopAnimating()
                self.collectionView.reloadData()
                self.tblListView.reloadData()
            }
        }
    }
    
    override func objectSelected(object: AnyObject, isSelect: String) {
        isSelectView = isSelect
        if let prize = object as? Prize{
            if (prize.amount == 0){
                UIUtils.toast("No quedan unidades disponible")
            }else{
                self.performSegue(withIdentifier: self.segueOnCellClickedName, sender: self)
            }
        }
    }
    
    
//    override
//    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
//
//    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.segueOnCellClickedName {
            if let prize = self.selectedObject as? Prize{
                let prizeController = segue.destination as! PrizeViewController
                prizeController.prize = prize
                prizeController.topCredit = self.creditsLabel.text!
            }
        }
    }
    
    @IBAction func leftMenuButtonPressed(_ sender: AnyObject) {
        self.slideMenuController()?.openLeft()
    }
    
    func loadUser(_ user: User){
        self.creditsLabel.text = "\(UIUtils.clean(user.credits))"
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
}

extension PrizesViewController {
    
    @IBAction func clickOnBtnList(_ sender : UIButton)
    {
        self.collectionView.isHidden = true
        self.tblListView.isHidden = false
        //btnList.isSelected = true
        //btnGrid.isSelected = false
        
        let origImage = UIImage(named: "grid_dark")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        btnGrid.setImage(tintedImage, for: .normal)
        btnGrid.tintColor = UIColor.lightGray
        btnList.setImage(UIImage(named: "list_dark"), for: UIControl.State.normal)
    }
    
    @IBAction func clickOnBtnGrid(_ sender : UIButton)
    {
        self.collectionView.isHidden = false
        self.tblListView.isHidden = true
        
        let origImage = UIImage(named: "list_dark")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        btnList.setImage(tintedImage, for: .normal)
        btnList.tintColor = UIColor.lightGray
        btnGrid.setImage(UIImage(named: "grid_dark"), for: UIControl.State.normal)
    }
}
