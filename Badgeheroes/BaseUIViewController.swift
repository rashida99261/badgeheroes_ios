//
//  BaseUIViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/23/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class BaseUIViewController: UIViewController {

    public var loader: NVActivityIndicatorView!
    
    override func viewDidLoad() {
       self.loader = UIUtils.addLoader(self.view, y: nil)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // NotificationCenter.default.addObserver(self, selector: #selector(BaseUIViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(BaseUIViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        
        
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let oldY = self.view.frame.origin.y
            
            if oldY == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
            
        }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        
        if ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue != nil {
            
            let oldY = self.view.frame.origin.y
            if oldY != 0{
                self.view.frame.origin.y = 0
            }
            
        }
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
}
