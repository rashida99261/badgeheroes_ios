////
////  MissionsViewController.swift
////  copyapp
////
////  Created by Gonzalo Lopez on 5/18/16.
////  Copyright © 2016 MisPistachos. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//import NVActivityIndicatorView
//
//class CampaignsViewController: BCollectionIndexViewController {
//
//    @IBOutlet weak var collectionView: UICollectionView!
//    
//    @IBOutlet weak var scrollview: UIScrollView!
//    @IBOutlet weak var toolbar: UIToolbar!
//   
//    var loader: NVActivityIndicatorView!
//   
//    override func viewDidLoad() {
//        self.objectsCollectionView = self.collectionView
//        self.categoriesScrollview = self.scrollview
//        self.categoriesToolbar = self.toolbar
//        self.segueOnCellClickedName = "CampaignToMissionsSegue"
//        
//        
//        
//        super.viewDidLoad()
//        
//        self.loader = UIUtils.addLoader(self.view,y: nil)
//        loader.startAnimating()
//        
//        
//        
//        
//        
//        // Do any additional setup after loading the view.
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
//        
//    }
//    
//    
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//    }
//    
//    
//    
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//        //self.navigationController?.setNavigationBarHidden(false, animated: animated)
//    }
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//   
//    
//    override func fillCell(_ cell: UICollectionViewCell, object: AnyObject) -> UICollectionViewCell {
//        let item = cell as! BCampaignCell
//        let campaign = object as! Campaign
//        
//        item.campaignName.text = campaign.name
//        item.campaignImage.kf.setImage(with: URL(string: campaign.imageUrl! )!)
//        item.progressLabel.text = "\(campaign.completedMissions!)/\(campaign.totalMissions!)"
//        /*if let cid = campaign.campaignCategoryId{
//            item.categoryId = cid
//        }else{
//            item.categoryId = -2
//        }*/
//        
//        
//        switch campaign.State {
//        case .available:
//            item.toAvailable()
//        case .locked:
//            item.toLocked()
//        case .accepted:
//            item.toAccepted()
//        default:
//            print("Campaign state not found")
//        }
//        
//       
//        //item.backgroundColor = campaign.Color
//        
//        return item
//    }
//    
//    
////    func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
////        
////        let item = collectionView.cellForItemAtIndexPath(indexPath) as! BCampaignCell
////        item.cellPressed()
////        
////    }
////    
////    func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
////        
////        let item = collectionView.cellForItemAtIndexPath(indexPath) as! BCampaignCell
////        
////        collectionView.cell
////        item.cellReleased()
////    }
//
//    
//    override func objectCellName(object: AnyObject) -> String {
//        return "CampaignCell"
//    }
//    
//    override func loadData(){
//        CampaignsAPI.getCampaigns(userId: Settings.UserId) { (campaignsResponse: CampaignsResponse?, error: NSError?) in
//            
//            if let serverCampaigns = campaignsResponse?.campaigns{
//                
//                self.totalObjects = serverCampaigns.sorted(by: { $0.Position < $1.Position })
//                self.objects = self.totalObjects
//                
//                self.collectionView.reloadData()
//                
//                self.loader.stopAnimating()
//                
//            }
//            
//            if let categories = campaignsResponse?.categories{
//                self.setCategoryItems(categories)
//            }
//            
//           
//        }
//    }
//   
//    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        
//            if segue.identifier == self.segueOnCellClickedName {
//                if let campaign = self.selectedObject as? Campaign{
//                    let missionsController = segue.destination as! MissionsViewController
//                    missionsController.campaign = campaign
//                }
//                
//            }
//        
//    }
//
//}
