//
//  DefaultModalViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/1/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class DefaultModalViewController: BModalViewController {
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet var generalView: BTabView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var containerView: UIView!
    var embeddedController: UIViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSubController()
        setFontAwesome()
        UIUtils.loadLogo(logoImageView)
      
        self.generalView.backgroundColor = BColor.PrimaryBar
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        if let mf = self.modalFinished{
            mf()
        }
    }
    
    fileprivate func setSubController(){
        
        embeddedController.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChild(embeddedController)
        UIUtils.addSubView(embeddedController.view, toView: self.containerView)
        
        
    }
    
    func setFontAwesome(){
        let attributes = [NSAttributedString.Key.font: UIFont.fontAwesome(ofSize: 20, style: .regular)] as Dictionary
        
        backButton.setTitleTextAttributes(attributes, for: UIControl.State())
        backButton.title = String.fontAwesomeIcon(name: .chevronLeft)
    }
}
