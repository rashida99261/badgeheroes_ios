//
//  BCollectionIndexViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/6/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import ObjectMapper
import UIKit

class BCollectionIndexViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout , UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate{
    
    var objectsCollectionView: UICollectionView!
    var objectTableView: UITableView!
    var searchText: UITextField!
    var searchActive : Bool = false
    
    var imgBonus: UIImageView!
    var imgBonysHt: NSLayoutConstraint!
    
    //Tienen que ser Mappables y heredar de BCategorizable !!
    var objects: [AnyObject] = [AnyObject]()
    var searchObjects: [AnyObject] = [AnyObject]()
    
    var totalObjects: [AnyObject] = [AnyObject]()
    var selectedObject: AnyObject?
    var selectedCategoryButton: UIBarButtonItem?
    
    ////static let campaignToMissionsSegue = "CampaignToMissionsSegue"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func getWidth(index: Int) -> CGFloat{
        //OVERRIDE
        return self.view.frame.size.width
    }
    
    func getHeight(index: Int) -> CGFloat{
        //OVERRIDE
        return (self.view.frame.size.width / 2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let tabBarController = (self.tabBarController as? TabBarController){
            tabBarController.setNavbarColor(nil)
            tabBarController.enableBackButton(false, navController: nil)
            tabBarController.setNavigationTitle(nil)
        }
        //self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return self.objects.count
        
        if(searchActive) {
            return self.searchObjects.count
        }
        return self.objects.count
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        
        let index: Int = (indexPath as NSIndexPath).row
        //let object = objects[index]
        var object : AnyObject!
        if(searchActive){
            object = searchObjects[index]
        }else{
            object = objects[index]
        }
        
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: self.objectCellName(object: object), for: indexPath)
        let _ = self.fillCell(item, object: object)
        return item
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let index: Int = (indexPath as NSIndexPath).row
        let width = getWidth(index: index)
        let height = getHeight(index: index)
        
        return CGSize(width: width, height: height)
//        if(index == 0){
//            let width = collectionView.frame.size.width
//            let height = getHeight(index: index)
//            return CGSize(width: width, height: collectionView.frame.size.width / 2)
//        }
//        else{
//            let width = (collectionView.frame.size.width - 30) / 2//  getWidth(index: index)        //
//            let height = getHeight(index: index)
//            return CGSize(width: width, height: 200)
//        }
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 10
//    }
//
//    //--------------------------------------------------------------------------------
//
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1;
    }
    
    //--------------------------------------------------------------------------------
    
   
    //--------------------------------------------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index: Int = (indexPath as NSIndexPath).row
        self.selectedObject = objects[index]
        
        if let so = selectedObject{
            objectSelected(object: so, isSelect: "collection")
        }
    }
    
    func objectSelected(object: AnyObject,isSelect: String){
    }
    
//    func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
//        
//        let item = collectionView.cellForItem(at: indexPath)
//        item!.backgroundColor = UIColor.clear//BColor.LockedCellBackground
//        
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
//        
//        let item = collectionView.cellForItem(at: indexPath)
//        item!.backgroundColor = UIColor.clear
//    }
    
    func loadData(){
        //OVERRIDE !
    }
    
    func fillCell(_ cell: UICollectionViewCell, object: AnyObject) -> UICollectionViewCell{
        //OVERRIDE !
        return cell
    }
    
    func objectCellName(object: AnyObject) -> String{
        //OVERRIDE !
        return ""
    }
    
    //Es llamado o por el scrollview, o por el mismo boton. Buscar con CTRL+F donde es llamado
    func categoryButtonPressed(_ sender: UIBarButtonItem){
        let id = sender.tag
        
        if id == -1{
            self.objects = self.totalObjects
            
            
        }else{
            var objects = [AnyObject]()
            self.totalObjects.forEach { (o) in
                if let oCategoryId = (o as? BCategorizable)?.categoryId{
                    if oCategoryId == id{
                        objects.append(o)
                    }
                }
            }
            self.objects = objects
        }
        
        self.selectedCategoryButton?.tintColor = BColor.TextButtonNormal
        sender.tintColor = BColor.TextButtonPressed
        self.selectedCategoryButton = sender
        
        self.objectsCollectionView.reloadData()
        self.objectTableView.reloadData()
        
        
        
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if self.selectedObject != nil{
            return true
        }else{
            print("Error en la selección de campaña")
            return false
        }
    }
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
  
    
    
    //tableview code
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(searchActive) {
            return self.searchObjects.count
        }
       return self.objects.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        let index: Int = (indexPath as NSIndexPath).section
        
        var object : AnyObject!
        if(searchActive){
             object = searchObjects[index]
        }else{
             object = objects[index]
        }
        
        let cell = objectTableView.dequeueReusableCell(withIdentifier: "tblPrizeCell") as! tblPrizeCell
        let prize = object as! Prize
        
        cell.tbl_prizeName.text = prize.name
        if let iurl = prize.imageUrl{
            cell.tbl_imageView.kf.setImage(with: URL(string: iurl )!)
        }
        cell.tbl_credits.text = "\(prize.credits!)"
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index: Int = (indexPath as NSIndexPath).section
        self.selectedObject = objects[index]
        if let so = selectedObject{
            objectSelected(object: so,isSelect: "table")
        }
    }
    
    
    
    ////////********** search code*******\\\\\\\\\\\
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchActive = true;
        textField.returnKeyType = .search
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == searchText){
            self.searchText.resignFirstResponder()
            
            if (self.searchText.text == ""){
                searchActive = false
                objectTableView.reloadData()
                objectsCollectionView.reloadData()
            } else {
                searchActive = true
                searchObjects.removeAll()
                for i in 0...objects.count-1{
                    let object = objects[i]
                    let prize = object as! Prize
                    
                    let tmp = prize.name! as NSString
                    let range = tmp.range(of: self.searchText.text!, options: NSString.CompareOptions.caseInsensitive)
                    
                    if(range.location != NSNotFound){
                        searchObjects.append(object)
                    }else{
                    }
                }
            }
            objectsCollectionView.reloadData()
            objectTableView.reloadData()
            return true
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchActive = false;
    }
    
    
//    func filteredArray(searchString:NSString){// we will use NSPredicate to find the string in array
//        let predicate = NSPredicate(format: "SELF contains[c] %@",searchString) // This will give all element of array which contains search string
//        //let predicate = NSPredicate(format: "SELF BEGINSWITH %@",searchString)// This will give all element of array which begins with search string (use one of them)
//        filteredArray = shoppingList.filteredArrayUsingPredicate(predicate)
//        print(filteredArray)
//    }
    
    
}
