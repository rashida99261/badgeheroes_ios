//
//  Color.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/2/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import UIKit
import SwiftHEXColors

open class BColor {
    
    open static var IntroBackgroundColor: UIColor!
    
    open static var PrimaryBar: UIColor!
    open static var SecondaryBar: UIColor!
    
    open static var TextButtonNormal: UIColor!
    open static var TextButtonPressed: UIColor!
    
    
    open static var ButtonNormal: UIColor!
    open static var ButtonPressed: UIColor!
    
    open static var ButtonPrimary: UIColor!
    open static var ButtonPrimaryPressed: UIColor!
    
    open static var ButtonNormalText: UIColor!
    open static var ButtonPressedText: UIColor!
    
    
    open static var ComplementButtonNormalText: UIColor!
    open static var ComplementTextButtonPressedText: UIColor!
    
    open static var ComplementButtonNormal: UIColor!
    open static var ComplementButtonPressed: UIColor!
    
    open static var Label: UIColor!
    
    open static var Light: UIColor!
    open static var Dark: UIColor!
    open static var Green: UIColor!
    open static var SuccessGreen: UIColor!
    open static var MissingGray: UIColor!
    
    open static var Red: UIColor!
    open static var WrongRed: UIColor!
    
    open static var TabBackground: UIColor!
    
    
    open static var LockedCellBackground: UIColor!
    open static var UnlockedCellBackground: UIColor!
    
    open static var TextInputBackground: UIColor!
    open static var TextInputBorder: UIColor!
    
    open static var LoaderColor: UIColor!
    open static var NotificationBackgroundColor: UIColor!
    
    open static var GoldPrimary: UIColor!
    open static var GoldSecondary: UIColor!
    
    
    
    open static func toDefaultColors(){
        IntroBackgroundColor = UIColor(patternImage: UIImage(named: "blue_background")!)
        
        PrimaryBar = UIColor(hexString: "#E0FEF0")!
        SecondaryBar = UIColor(hexString: "#082966")!
        
        TextButtonNormal = UIColor(hexString: "#ffffff")!
        TextButtonPressed = UIColor(hexString: "#ffa600")!
        
        
        ButtonNormal = UIColor(hexString: "#ffa600")!
        ButtonPressed = UIColor(hexString: "#ffa600", alpha: 0.5)!
        
        ButtonPrimary = UIColor(hexString: "#005578")!
        ButtonPrimaryPressed = UIColor(hexString: "#005578", alpha: 0.5)!
        
        ButtonNormalText = UIColor(hexString: "#ffffff")!
        ButtonPressedText = UIColor(hexString: "#de5c18")!
        
        
        ComplementButtonNormalText = UIColor(hexString: "#ffffff")!
        ComplementTextButtonPressedText = UIColor(hexString: "#f2881d")!
        
        ComplementButtonNormal = UIColor(hexString: "#FAB40A")!
        ComplementButtonPressed = UIColor(hexString: "#FAB40A", alpha: 0.5)!
        
        Label = UIColor(hexString: "#ffa600")!
        
        Light = UIColor(hexString: "#ffffff")!
        Dark = UIColor(hexString: "#000000")!
        Green = UIColor(hexString: "#1e7b1e")!
        Red = UIColor(hexString: "#e60000")!
        
        SuccessGreen = UIColor(hexString: "#23a455")!
        WrongRed = UIColor(hexString: "#e62c21")!
        MissingGray = UIColor(hexString: "#686868")!
        TabBackground = UIColor(hexString: "#ebebeb")!
        
        
        LockedCellBackground = UIColor(hexString: "#000000", alpha: 0.5)!
        UnlockedCellBackground = UIColor(hexString: "#ffffff")!
        
        TextInputBackground = UIColor(hexString: "#f9f9f9")!
        TextInputBorder = UIColor(hexString: "#c0c3c0")!
        
        LoaderColor = UIColor(hexString: "#bdbdbd")!
        
        NotificationBackgroundColor = UIColor(hexString: "#3c3c3c")
        
        GoldPrimary = UIColor(hexString: "#f4bd44")!
        GoldSecondary = UIColor(hexString: "#d26d2a")!
    }
    
    open static func reloadColors(){
        if Settings.PrimaryColor != ""{
            BColor.PrimaryBar = UIColor(hexString: Settings.PrimaryColor)!
            
        }
        
        if Settings.SecondaryColor != ""{
            BColor.SecondaryBar = UIColor(hexString: Settings.SecondaryColor)!
        }

        
        if Settings.ElementColor != ""{
            BColor.ButtonNormal = UIColor(hexString: Settings.ElementColor)!
            BColor.ButtonPressed = UIColor(hexString: Settings.ElementColor, alpha: 0.5)!
            BColor.TextButtonPressed = UIColor(hexString: Settings.ElementColor)!
            BColor.Label = UIColor(hexString: Settings.ElementColor)!
        }
        
        
        if Settings.ElementTextColor != ""{
            BColor.ButtonNormalText = UIColor(hexString: Settings.ElementTextColor)!
            
        }
        
        if Settings.ElementSecondaryColor != ""{
            
            BColor.ComplementButtonNormal = UIColor(hexString: Settings.ElementSecondaryColor)!
            BColor.ComplementButtonPressed = UIColor(hexString: Settings.ElementSecondaryColor, alpha: 0.5)!
        }
        
        if Settings.ElementSecondaryTextColor != ""{
            BColor.ComplementButtonNormalText = UIColor(hexString: Settings.ElementSecondaryTextColor)!
        }
    }

}

