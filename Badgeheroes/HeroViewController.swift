//
//  HeroViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/18/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import SwiftEventBus

class HeroViewController: BaseUIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var img_waves: UIImageView!
    
    
    @IBOutlet weak var headerLineView: UIView!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var progrssBar : UIProgressView!
    
    @IBOutlet weak var medalsViewContainer: UIView!
   // @IBOutlet weak var missionsViewContainer: UIView!
   // @IBOutlet weak var medalsTable: UITableView!
  //  @IBOutlet weak var missionsTable: UITableView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
//    @IBOutlet weak var userTeam: UILabel!
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var creditsLabelHeader: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    //@IBOutlet weak var medalsTableHeightConstraint: NSLayoutConstraint!
   // @IBOutlet weak var missionTableHeightConstraint: NSLayoutConstraint!
    
    var hideBackButton = true
    
    var userId: Int?
    var medals = [Medal]()
    var missions = [Mission]()
    var ranking = [UserRanking]()
    
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
//    @IBOutlet weak var creditsLabel: UILabel!
    
    
    //////******Profile collection outlets*********\\\\\\\\
    @IBOutlet weak var collProfileList: UICollectionView!
    var arrProfileData : NSMutableArray = []
    
    @IBOutlet weak var collMedalsList: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.reloadLogo()
        
        UIUtils.toCircleImage(userImage)
//        UIUtils.addShadowToCellView(self.missionsContainer, opacity: 0.1)
//        UIUtils.addShadowToCellView(self.medalsContainer, opacity: 0.1)
        
        //self.medalsTable.allowsSelection = false
        //self.medalsTableHeightConstraint.constant = 0
        
        self.loader.startAnimating()
        progrssBar.layer.cornerRadius = 6
        progrssBar.layer.masksToBounds = true
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navBarView.backgroundColor = BColor.PrimaryBar
        GlobalBgColor = BColor.PrimaryBar
  
        if hideBackButton{
            if let tabBarController = (self.tabBarController as? TabBarController){
                tabBarController.setNavbarColor(nil)
                tabBarController.enableBackButton(false, navController: nil)
                tabBarController.setNavigationTitle(nil)
                
            }
        }else{
            if let tabBarController = (self.tabBarController as? TabBarController){
                tabBarController.setNavbarColor(nil)
                tabBarController.enableBackButton(true, navController: self.navigationController)
                tabBarController.setNavigationTitle(nil)
            }
        }
        
       // self.headerView.backgroundColor = BColor.PrimaryBar
        
        self.img_waves.image = img_waves.image?.withRenderingMode(.alwaysTemplate)
        self.img_waves.tintColor = BColor.PrimaryBar
        
      //  self.setStaticProfileData()
      //  self.collProfileList.reloadData()
        
        //self.medalsTable.backgroundColor = BColor.Light
       // self.missionsTable.backgroundColor = BColor.Light
        self.headerLineView.backgroundColor = BColor.PrimaryBar
        if let id = self.userId{
            UsersAPI.getUser(userId: id, { (serverUser: User?, error:  NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                    self.loader.stopAnimating()
                }
            })
        }else{
            UsersAPI.getProfile { (serverUser: User?, error: NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                    self.loader.stopAnimating()
                }
            }
        }
    }
    
    func loadUser(_ user: User){
        
        if let medals = user.medals, let missions = user.missions, let rankng = user.user_ranking {
            self.medals = medals
            self.missions = missions
            self.ranking = rankng
            
            if let imageUrl = user.imageUrl{
                
                self.userImage.kf.setImage(with: URL(string: imageUrl)!)
            }
            
           // let cellHeight = 18 + 18 + 60
            
         //   missionTableHeightConstraint.constant = CGFloat(cellHeight*missions.count)
            
           // print(medals.count)
            
            if(medals.count == 0){
                self.contentViewHeightConstraint.constant = self.contentView.frame.height - 210
            }
            else{
                self.contentViewHeightConstraint.constant = self.contentView.frame.height
                self.collMedalsList.reloadData()
            }
            
            if(rankng.count == 0){
                self.contentViewHeightConstraint.constant = self.contentView.frame.height - 210
            }
            else{
                self.contentViewHeightConstraint.constant = self.contentView.frame.height
                self.collProfileList.reloadData()
            }
           // self.missionsTable.reloadData()
        }
        var sufix = ""
        if Settings.IsStagingMode == true{
            sufix = " - TEST"
        }
        
        if let title = user.title , title.name != nil{
            
            self.userName.text = "\(UIUtils.clean(title.name?.uppercased()))\(sufix)"
        }else{
            self.userName.text = "\(UIUtils.clean(user.FullName.uppercased()))\(sufix)"
        }
        
      //  self.userTeam.text = UIUtils.clean(user.team?.name)
        self.levelLabel.text = "\(UIUtils.clean(user.level))"
        progrssBar.progress = Float(UIUtils.clean(user.level))/Float(10)
        self.expLabel.text = "\(UIUtils.clean(user.experience))"
//        self.creditsLabel.text = "\(UIUtils.clean(user.credits))"
        self.creditsLabelHeader.text = "\(UIUtils.clean(user.credits))"
    }

    @IBAction func leftMenuButtonPressed(_ sender: AnyObject) {
        self.slideMenuController()?.openLeft()
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
    func setStaticProfileData()
    {
        let dict1 = ["title": "1", "image": "pic1", "name":"Ronaldo Alfredo","number":"42432"]
        let dict2 = ["title": "2", "image": "pic2", "name":"Riham Di Coltosto","number":"2324"]
        let dict3 = ["title": "3", "image": "pic5", "name":"Samentha Cordo","number":"12323"]
        let dict4 = ["title": "4", "image": "pic4", "name":"Julie Einstien","number":"244"]
        let dict5 = ["title": "5", "image": "pic1", "name":"Ronaldo Alfredo","number":"42432"]
        let dict6 = ["title": "6", "image": "pic6", "name":"Riham Di Coltosto","number":"2324"]
        let dict7 = ["title": "7", "image": "pic5", "name":"Samentha Cordo","number":"12323"]
        let dict8 = ["title": "8", "image": "pic4", "name":"Julie Einstien","number":"244"]
    
        arrProfileData.add(dict1)
        arrProfileData.add(dict2)
        arrProfileData.add(dict3)
        arrProfileData.add(dict4)
        arrProfileData.add(dict5)
        arrProfileData.add(dict6)
        arrProfileData.add(dict7)
        arrProfileData.add(dict8)
    }
    
    ////******* coll view method***\\\
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        
        if collectionView == self.collProfileList{
            //Profile
            return self.ranking.count
        }
        //Missions
        return self.medals.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        var item : UICollectionViewCell!
        if collectionView == collProfileList {
            //Profile
            let cell = collProfileList.dequeueReusableCell(withReuseIdentifier: "profileCollViewCell", for: indexPath) as! profileCollViewCell
            let rank = ranking[indexPath.row] //as! NSDictionary
            
            cell.lblTitle.text = "\(rank.position ?? 0)"
            cell.lblName.text = "\(rank.firstname!)"   //\(rank.lastname!)
            cell.lblNumber.text = "\(rank.experience ?? 0)"
            cell.imgProfilePic.layer.cornerRadius = cell.imgProfilePic.frame.width/2
            cell.imgProfilePic.layer.masksToBounds = true
            if let _ = rank.imageUrl
            {
                cell.imgProfilePic.kf.setImage(with: URL(string: rank.imageUrl!)!)
            }
            else{
                cell.imgProfilePic.image = UIImage(named: "user_icon")
            }
            item = cell
        }
        else{
            //Missions
            let index: Int = (indexPath as NSIndexPath).row
            let medal = medals[index]
            let bitem = collMedalsList.dequeueReusableCell(withReuseIdentifier: "medalCollCell", for: indexPath) as! medalCollCell
            //let imageView =  as! BItemImageView
            bitem.bimage.initImage(medal, delegateController: self)
            bitem.bimage.modal = false
            bitem.name.text = medal.name!
            item = bitem
        }
        return item
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == self.collProfileList{
            let yourWidth = collProfileList.bounds.width/4.0
            return CGSize(width: yourWidth, height: collProfileList.bounds.height)
        }
        else{
            let yourWidth = collMedalsList.bounds.width/4.0
            return CGSize(width: yourWidth, height: collMedalsList.bounds.height)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 4, bottom: 4, right: 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

}

class profileCollViewCell : UICollectionViewCell
{
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgProfilePic : BItemImageView!
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblNumber : UILabel!
}


class medalCollCell: UICollectionViewCell
{
    
    @IBOutlet weak var bimage: BItemImageView!
    @IBOutlet weak var name: UILabel!
    
    
}
