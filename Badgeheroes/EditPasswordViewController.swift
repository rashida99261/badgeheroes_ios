//
//  EditPasswordViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 11/22/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class EditPasswordViewController: UIViewController {

    @IBOutlet weak var oldPasswordField: BUITextField!
    
    @IBOutlet weak var submitButton: BSecondaryButton!
    @IBOutlet weak var newPasswordField: BUITextField!
    @IBOutlet weak var confirmPasswordField: BUITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        if let oldPassword = oldPasswordField.text, let newPassword = newPasswordField.text, let confirmationPassword = confirmPasswordField.text{
            
            if newPassword != confirmationPassword{
                self.submitButton.unlockButton()
                UIUtils.toast("La contraseña no coincide. Intentalo denuevo")
                newPasswordField.text = ""
                self.confirmPasswordField.text = ""
            }else if newPassword.count < 4 {
                self.submitButton.unlockButton()
                UIUtils.toast("La nueva contraseña debe tener al menos 4 caracteres")
            }else{
                UsersAPI.changePassword(old: oldPassword, new: newPassword, { (apiResponse, error) in
                    
                    self.submitButton.unlockButton()
                    if error != nil{
                        UIUtils.toast("Hubo un error de conexion")
                    }else if let ar = apiResponse{
                        if ar.error == false{
                            UIUtils.toast("Contraseña cambiada")
                            self.dismiss(animated: true, completion: nil)
                        }else{
                            UIUtils.toast("La contraseña actual es incorrecta. Contacta al administrador")
                            self.oldPasswordField.text = ""
                        }
                    }
                })
            }
            
            
        }else{
            UIUtils.toast("Debes rellenar todos los campos para cambiar tu contraseña")
        }
        
        
        
    }

}
