//
//  BPageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class BPageViewContainerController: UIViewController, MissionPageController {
    
    var pagePosition: Int!
    var totalPage: Int!
    var missionPage: MissionPage!
    var pageView: UIView!
    var missionId: Int!
    var watchRightAnswers: Bool! = false
    var watchUserAnswers: Bool! = false
    
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var pageLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var containerView: UIView!
    
    var missionPageController: MissionPageController!
    var missionPageViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // self.navigationController?.setNavigationBarHidden(true, animated: fals
        
        progressBar.layer.cornerRadius = 6
        progressBar.clipsToBounds = true
        progressBar.progressTintColor = GlobalBgColor?.darker(by: 13)
        let percentage : Float = (Float(pagePosition! + 1)) / (Float(totalPage!))
        progressBar.setProgress(percentage, animated: false)
        //  print("page count = \(totalPage!)-----\(pagePosition!)-------------\(percentage)")
        
        pageLabel.text = "\(pagePosition! + 1) DE \(totalPage!)"
        
        let pageType = missionPage.PageType
        let controllerIdentifier = "\(pageType.rawValue.capitalized)ViewController"
        
        switch missionPage.PageType {
        case .NotFound:
            print("No se ha implementado un controlador para ese tipo de página")
            missionPageController = nil
            missionPageViewController = nil
        default:
            missionPageViewController = self.storyboard?.instantiateViewController(withIdentifier: controllerIdentifier)
            missionPageController = (missionPageViewController as! MissionPageController)
            missionPageController.missionPage = self.missionPage
            missionPageController.pageView = self.view
            missionPageController.missionId = self.missionId
            missionPageController.watchRightAnswers = self.watchRightAnswers
            missionPageController.watchUserAnswers = self.watchUserAnswers
        }
        missionPageViewController?.loadViewIfNeeded()
        
        if let mpvc = missionPageViewController{
            setSubController(mpvc)
        }
        self.updateInformation()
        self.loadDefault()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.informationLabel.layoutIfNeeded()
        self.view.layoutIfNeeded()
        self.containerView.layoutIfNeeded()
    }
    
    func loadDefault(){
        missionPageController?.loadDefault()
    }
    
    fileprivate func updateInformation(){
        
        if("\(missionPage.PageType.rawValue.capitalized)" == "Message"  || "\(missionPage.PageType.rawValue.capitalized)" == "Video"){
            self.informationLabel.text = ""
            //let cf = self.containerView.frame
            //self.informationLabel.frame = CGRect(x: 0, y: 0, width: cf.width, height: 0)
            //self.informationLabel.frame.size = CGSize(width: cf.width, height: 0)
            //self.containerView.frame = CGRect(x: 0, y: self.informationLabel.frame.height + 5, width: cf.width, height: cf.height)
        }
        else{
            
            let text = missionPage.information
            var labelHeight: CGFloat?
            var newSize: CGRect?
            if let t = text{
                let text = UIUtils.toNSAttributedString(htmlString: t, fontSize: 17)
                self.informationLabel.attributedText = text
                newSize = UIUtils.getContainerSizeFor(text!, labelSize: self.informationLabel.frame)
                labelHeight = newSize!.height
            }
            
            if labelHeight == nil{
                labelHeight = self.informationLabel.frame.height
            }
            self.informationLabel.sizeToFit()
//
            let cf = self.containerView.frame
//            //self.informationLabel.frame = CGRect(x: 0, y: 0, width: cf.width, height: labelHeight!)
            self.informationLabel.frame.size = CGSize(width: cf.width, height: labelHeight!)
            self.containerView.frame = CGRect(x: 0, y: labelHeight! + 5, width: cf.width, height: cf.height)
            
        }
        
    }
    
    fileprivate func setSubController(_ viewController: UIViewController){
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.addChild(viewController)
        UIUtils.addSubView(viewController.view, toView: self.containerView)
           // viewController.loadView()
    }
    
    func completeMissionPage() -> MissionPage{
        return missionPageController.completeMissionPage()
    }
    
    func isPageCompleted() -> Bool{
        return missionPageController.isPageCompleted()
    }
    
    func isPageCorrect() -> Bool{
        return missionPageController.isPageCorrect()
    }
    
    func showErrorMessage() {
        return missionPageController.showErrorMessage()
    }
    
    @IBAction func clickOnBackBtn(_ sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
