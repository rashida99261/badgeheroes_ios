//
//  MainViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.

import UIKit
import SlideMenuControllerSwift


class MainViewController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
        SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.width*0.8
//        needsUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if !Settings.IsWelcomeDone{
            let welcomeController = self.storyboard!.instantiateViewController(withIdentifier: "WelcomePageViewController") as! WelcomePageViewController
            
            welcomeController.modalFinished = {
                Settings.IsWelcomeDone = true
                self.dismiss(animated: true, completion: nil)
            }
            self.present(welcomeController, animated: true, completion: nil)
        }
        
        if !Settings.IsLoggedIn{
            let loginContainerController = self.storyboard!.instantiateViewController(withIdentifier: "LoginContainerViewController") as! LoginContainerViewController
            loginContainerController.modalFinished = {
                self.dismiss(animated: true, completion: nil)
            }
            self.present(loginContainerController, animated: true, completion: nil)
        }
        
        
        if !Settings.HelloMessageDone{
            if Settings.IsLoggedIn{
                
               // let personalHelloViewController = self.storyboard!.instantiateViewController(withIdentifier: "RewardPopupController") as! RewardPopupController
                let personalHelloViewController = self.storyboard!.instantiateViewController(withIdentifier: "AfterLoginPopUpVC") as! AfterLoginPopUpVC
                personalHelloViewController.credits = 0
                personalHelloViewController.strTitle = "¡Hola \(Settings.FirstName)!"
                personalHelloViewController.fromLocal = true
                personalHelloViewController.modalPresentationStyle = .overCurrentContext
                
                personalHelloViewController.modalFinished = {
                    Settings.HelloMessageDone = true
                    self.dismiss(animated: true, completion: nil)
                }
                
                self.present(personalHelloViewController, animated: true, completion: nil)
                
            }
        }
    }
    override var shouldAutorotate : Bool {
        return false
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    
    override func awakeFromNib() {
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") {  //TabBarController  ContainerViewController
            self.mainViewController = controller
            //self.mainViewController?.loadViewIfNeeded()
            
        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "LeftBarViewController") {
            self.leftViewController = controller
            //self.leftViewController?.loadViewIfNeeded()
        }
        super.awakeFromNib()
    }
    
}
