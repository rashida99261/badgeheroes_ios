
//
//  EmailValidatorViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/20/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import Alamofire


class EmailValidatorViewController: BModalViewController {

    @IBOutlet weak var userTextField: BUITextField!
    @IBOutlet weak var passwordField: BUITextField!

    // Center Popup outlets
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerPopupVW: UIView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var helpButton: UIButton!
    
    var user: User?
    
    @IBOutlet weak var acceptButton: BSecondaryButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.enableNavbar?(false)
        
        userTextField.text = "swati@appsontechnologies.in" //"amphora@amphora.cl"  //"swati@appsontechnologies.in" //"nuevo@rateinc.cl" // //"swati@appsontechnologies.in"
        passwordField.text = "swati"
        helpButton.layer.cornerRadius = 22
        
        blurView.isHidden = true
        centerPopupVW.isHidden = true
        helpButton.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(showSpinningWheel(notification:)), name:NSNotification.Name(rawValue: "isDismissViewController"), object: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.acceptButton.unlockButton()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func showSpinningWheel(notification: NSNotification) {
        
        if let dict = notification.userInfo as NSDictionary? {
            if let str = dict["dismissfrom"] as? String{
                // do something with your image
                if(str == "callback"){
                    blurView.isHidden = true
                    centerPopupVW.isHidden = true
                    helpButton.isHidden = true
                }
                else{
                    self.blurView.isHidden = false
                    self.centerPopupVW.isHidden = false
                    self.helpButton.isHidden = false
                }
            }
        }
    }
    
    @IBAction func acceptButtonPressed(_ sender: AnyObject) {
    
        if(userTextField.text == "" || userTextField.text?.count == 0){
                UIUtils.toast("Ingresa tu email")
                self.acceptButton.unlockButton()
                
        }
        else if(!UIUtils.isValidEmail(testStr: userTextField.text!)){
                UIUtils.toast("Debes ingresar un correo valido")
                self.acceptButton.unlockButton()
        }
        else if(passwordField.text == "" || passwordField.text?.count == 0){
            UIUtils.toast("Ingresa tu contraseña")
            self.acceptButton.unlockButton()
        }
        else{
            UsersAPI.performLogin(email: userTextField.text!, password: passwordField.text!, callback: { (u: User?, e: NSError?) in
                
                if UIUtils.setApplicationUser(u: u, mf: self.modalFinished){
                    print("login")
                }
                else{
                   // print("error = \(u?.error_info)")
                    if (u?.error_info == "password")
                    {
                        UIUtils.toast("Usuario o contrasena erroneas")
                        self.acceptButton.unlockButton()
                    }
                    else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                            UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .curveEaseInOut], animations: { [weak self] in
                                self!.blurView.isHidden = false
                                self!.centerPopupVW.isHidden = false
                                self!.helpButton.isHidden = false
                                }, completion: { (_) in
                                    self.lblEmail.text = self.userTextField.text
                                    self.acceptButton.unlockButton()
                            })
                        }
                    }
                    }
            })
        }
//                UsersAPI.preLogin(email: text, { (u: User?, error: NSError?) in
//                    // Tiene id, email e imagen
//                    if let user = u {
//                        if user.Valid{
//                            Settings.UserId = user.id!
//                            Settings.ImageUrl = user.imageUrl!
//                            Settings.Email = user.email!
//
//                            let controller = self.storyboard?.instantiateViewController(withIdentifier: "PasswordValidatorViewController") as! PasswordValidatorViewController
//                            controller.modalFinished = self.modalFinished
//                            controller.enableNavbar = self.enableNavbar
//                            self.navigationController?.pushViewController(controller, animated: true)
//                            self.acceptButton.unlockButton()
//                            return
//                        }else{
//                            Settings.Email = text
//
//                            UIUtils.toast("Email no registrado")
//                            let controller = self.storyboard?.instantiateViewController(withIdentifier: "LoginContactController") as! LoginContactController
//                            controller.email = text
//                            //UIUtils.showInDefaultModal(self, controller: controller)
//                            controller.enableNavbar = self.enableNavbar
//
//                            self.navigationController?.pushViewController(controller, animated: true)
//                            self.acceptButton.unlockButton()
//                            return
//                        }
//
//                    }
//                    self.acceptButton.unlockButton()
//
//                })
    }
    
    @IBAction func clickOnForgetPasswordBtn(_ sender : UIButton){
        
        if(userTextField.text == "" || userTextField.text?.count == 0){
            UIUtils.toast("Ingresa tu email")
            self.acceptButton.unlockButton()
        }
        else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
            controller.email = userTextField.text!
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func closeCenterView(_ sender : UIButton)
    {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 1, delay: 0.0, options: [.curveLinear, .curveEaseInOut], animations: { [weak self] in
                
                self!.blurView.isHidden = true
                self!.centerPopupVW.isHidden = true
                self!.helpButton.isHidden = true
                
                }, completion: { (_) in
            })
        }
        
    }
    
    @IBAction func goForHelp(_ sender : UIButton)
    {
        let helpController = self.storyboard!.instantiateViewController(withIdentifier: "LoginContactController") as! LoginContactController
        helpController.email = userTextField.text!
        helpController.modalPresentationStyle = .overCurrentContext
        present(helpController, animated: true, completion: nil)
    }
}
