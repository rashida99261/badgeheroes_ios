//
//  BCampaignsViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/7/17.
//  Copyright © 2017 MisPistachos. All rights reserved.
//

import UIKit
import ObjectMapper
import NVActivityIndicatorView
import FontAwesome_swift
import SwiftEventBus

//BCollectionIndexViewController
class BCampaignsViewController: BCollectionIndexViewController {
    
    var userId: Int?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var navBarViewHeightConstraint : NSLayoutConstraint!
    
    // Center Popup outlets
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerPopupVW: UIView!
    @IBOutlet weak var iupsLabel: UILabel!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var redoButton: UIButton!
    @IBOutlet weak var centerImgView: UIImageView!
    
    //bonus
    @IBOutlet weak var imgBomusStar: UIImageView!
    @IBOutlet weak var imgBonusHeight: NSLayoutConstraint!
    
    var loader: NVActivityIndicatorView!
    var sizes: [[CGFloat]]!
    var timers: [Timer] = [Timer]()
    
    var campaignStr = ""
    let segueOnCellClickedName = "CampaignToMissionsSegue"
    
    override func viewDidLoad() {
        
        
        
        blurView.isHidden = true
        
        self.reloadLogo()
        
        self.objectsCollectionView = self.collectionView
        self.imgBonus = self.imgBomusStar
        self.imgBonysHt = self.imgBonusHeight
        self.imgBonus.isHidden = true
        self.imgBonysHt.constant = 0
        
        super.viewDidLoad()
        
        self.loader = UIUtils.addLoader(self.view,y: nil)
        loader.startAnimating()
        
        // Do any additional setup after loading the view.
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let id = self.userId{
            UsersAPI.getUser(userId: id, { (serverUser: User?, error:  NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            })
        }else{
            UsersAPI.getProfile { (serverUser: User?, error: NSError?) in
                if let su = serverUser{
                    self.loadUser(su)
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        strColl = "campaign"
        
        self.navBarView.backgroundColor = BColor.PrimaryBar
        GlobalBgColor = BColor.PrimaryBar
        
       // redoButton.layer.cornerRadius = 22
        
        if UIDevice().userInterfaceIdiom == .phone {
            navBarViewHeightConstraint.constant = UIUtils.getHeaderImageHeightForCurrentDevice()
        }
        
        if let _ = UserDefaults.standard.value(forKey: "subscribe"){
            //redoButton.isUserInteractionEnabled = false
            //redoButton.isEnabled = false
            redoButton.backgroundColor = UIColor(hexString: "969696")
            redoButton.setTitleColor(UIColor(hexString: "E1E1E1"), for: .normal)
            iupsLabel.text = "No tenemos actividades por el momento"
            msgLabel.text = "Pero no te preocupes, te contactaremos cuando tengamos más campañas para ti !"
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        for t in self.timers{
            t.invalidate()
        }
        self.timers = [Timer]()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func fillCell(_ cell: UICollectionViewCell, object: AnyObject) -> UICollectionViewCell {
        let campaign = object as! Campaign
        
        
        
        var item: BCampaignCell! = nil
        if campaign.FullMode{
            item = cell as! BSingleCampaignCell
            campaignStr = "single"
        }else{
            item = cell as! BMultiCampaignCell
            campaignStr = "multiple"
        }
        
        item?.setCampaign(campaign: campaign)
        
        if(isBonusPresent == "true"){
            self.imgBonus.isHidden = false
            self.imgBonysHt.constant = 40
        }
        else{
            self.imgBonus.isHidden = true
            self.imgBonysHt.constant = 0
        }
        
        switch campaign.State {
            case .available:
                item.toAvailable()
            case .locked:
                item.toLocked()
            case .expired:
                item.toExpired()
            case .completed:
                item.toAccepted()
            default:
                print("Campaign state not found")
            }
        if let t = item.timer{
            self.timers.append(t)
        }
        return item!
    }
    
    override func objectCellName(object: AnyObject) -> String {
        let campaign = object as! Campaign
        if campaign.FullMode{
            campaignStr = "single"
            return "BSingleCampaignCell"
        }else{
            campaignStr = "multiple"
            return "BMultiCampaignCell"
        }
    }
    
    override func getWidth(index: Int) -> CGFloat {
        if campaignStr == "single"{
            let width = collectionView.frame.size.width
            return width
        }
        else {
            
            if(self.sizes.count % 2  == 0){
                let width = (collectionView.frame.size.width - 30) / 2
                return width
            }
            else{
                if(index == self.sizes.count - 1){
                    // if self.sizes.count % 2  == 1 {
                    let width = collectionView.frame.size.width
                    return width
                }
                else{
                    let width = (collectionView.frame.size.width - 30) / 2
                    return width
                }
            }
            
            }
            
//            if(index == 0){
//                let width = collectionView.frame.size.width
//                return width
//            }
//            else {
            
//            }
        
    }
    
    override func getHeight(index: Int) -> CGFloat {
        
        if campaignStr == "single"{
             return 125
        }else{
             return 200
        }
    }
 
   
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
    }
    
    override func loadData(){
        CampaignsAPI.getCampaigns(userId: Settings.UserId) { (campaignsResponse: CampaignsResponse?, error: NSError?) in
            
           // print("campppppppp = \(campaignsResponse?.campaigns)")
            
            if let serverCampaigns = campaignsResponse?.campaigns, let campaignTree = campaignsResponse?.campaignTree {
                
                let campaignContainers: [CampaignContainer] = CampaignContainer.load(campaigns: serverCampaigns, campaignTree: campaignTree, screenWidth: self.view.frame.size.width)
                var sizes : [[CGFloat]] = []
                var campaigns : [Campaign] = []
                
                
            
                for campaignContainer in campaignContainers{
                    for campaign in campaignContainer.campaigns{
                        campaigns.append(campaign)
                        sizes.append([campaignContainer.campaignWidth(), campaignContainer.campaignHeight()])
                    }
                }
                self.sizes = sizes
                self.totalObjects = campaigns
                self.objects = self.totalObjects
                
                

                self.blurView.isHidden = true
                self.collectionView.isHidden = false
                
                
                if campaigns[0].FullMode{
                    self.campaignStr = "single"
                }else{
                    self.campaignStr = "multiple"
                }
                
                self.collectionView.reloadData()
                self.loader.stopAnimating()
                
                if (self.totalObjects.count == 0)
                {
                    self.collectionView.isHidden = true
                    self.blurView.isHidden = false
                }
            }
        } 
    }
    
    override func objectSelected(object: AnyObject,isSelect: String){
        if let campaign = object as? Campaign{
            switch campaign.State {
            case .available:
                self.performSegue(withIdentifier: self.segueOnCellClickedName, sender: self)
            case .expired:
                if campaign.BeginDateMessage != ""{
                    UIUtils.toast(campaign.BeginDateMessage)
                }else{
                    UIUtils.toast("Esta campaña ya no está disponible")
                }
            case .completed:
                UIUtils.toast("¡Ya completaste esta campaña!")
            case .locked:
                UIUtils.toast("Debes completar las campañas anteriores")
            default:
                UIUtils.toast("")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == self.segueOnCellClickedName {
            if let campaign = self.selectedObject as? Campaign{
                let missionsController = segue.destination as! MissionsViewController
                missionsController.campaign = campaign
            }
        }
    }
    
    @IBAction func leftMenuButtonPressed(_ sender: AnyObject) {
            self.slideMenuController()?.openLeft()
    }
    
    func loadUser(_ user: User){
        self.creditsLabel.text = "\(UIUtils.clean(user.credits))"
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
    @IBAction func closeCenterView(_ sender : UIButton)
    {
        blurView.isHidden = true
        centerPopupVW.isHidden = true
        redoButton.isHidden = true
        self.centerImgView.isHidden = true
    }
    
    @IBAction func redoMision(_ sender : UIButton)
    {
        if let _ = UserDefaults.standard.value(forKey: "subscribe"){
            UIUtils.toast("Ya te haz suscrito. Te notificaremos cuando tengamos nuevas campañas disponibles")
        }
        else{
            loader.startAnimating()
            CampaignsAPI.postSubscribe(reason: "no-campaings") { (response: ApiResponse?, error: NSError?) in
                
                if(response?.status == "OK"){
                    
                    self.loader.stopAnimating()
                    UIUtils.toast("¡Genial! Te notificaremos cuando tengas nuevas campañas disponibles")
                    
                    self.iupsLabel.text = "No tenemos actividades por el momento"
                    self.msgLabel.text = "Pero no te preocupes, te contactaremos cuando tengamos más campañas para ti !"
                    //sender.isUserInteractionEnabled = false
                    // sender.isEnabled = false
                    sender.backgroundColor = UIColor(hexString: "969696")
                    sender.setTitleColor(UIColor(hexString: "E1E1E1"), for: .normal)
                    UserDefaults.standard.set("press", forKey: "subscribe")
                }
            }
        }

        
    }
}
