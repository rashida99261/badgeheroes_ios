//
//  MedalViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/4/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class MedalViewController: BaseUIViewController {
    
    
    @IBOutlet weak var medalName: UILabel!
    @IBOutlet weak var medalImage: UIImageView!
    @IBOutlet weak var medalDescription: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var creditsIcon: UILabel!
    @IBOutlet weak var credits: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var medal: Medal?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lineView.backgroundColor = BColor.ButtonNormal
        self.creditsIcon.textColor = BColor.ButtonNormal
        self.scrollView.backgroundColor = BColor.TabBackground
        if let tb = self.tabBarController as? TabBarController{
            tb.setNavbarColor(nil)
            tb.setNavigationTitle(nil)
        }
        
        if let m = self.medal{
            
            MedalsAPI.getMedal(medalId: m.id!, callback: { (medal: Medal?, error: NSError?) in
                if let mm = medal{
                    if mm.id != nil{
                        self.medalName.text = UIUtils.clean(mm.name)
                        self.medalImage.kf.setImage(with: URL(string: mm.imageUrl!)!)
                        
                        self.credits.text = "\(UIUtils.clean(mm.credits))"
                        self.medalDescription.text = UIUtils.clean(mm.description)
                        self.medalDescription.sizeToFit()
                    }
                }
            })
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if let tb = self.tabBarController as? TabBarController{
            
            tb.enableBackButton(true, navController: self.navigationController)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}
