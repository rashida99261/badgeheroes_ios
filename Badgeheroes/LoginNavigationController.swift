//
//  LoginNavigationController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 7/22/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class LoginNavigationController: UINavigationController {
    var modalFinished: (() -> Void)?
    var enableNavbar: ((Bool) -> Void)?
    
    override func viewDidLoad() {
        for v in self.viewControllers{
            if let modalController = v as? BModalViewController{
                if let mf = self.modalFinished{
                    modalController.modalFinished = mf
                }
                
                if let en = self.enableNavbar{
                    modalController.enableNavbar = en
                }
                
            }
        }
    }
    
    func backButtonPressed(){
        self.popViewController(animated: true)
    }

}
