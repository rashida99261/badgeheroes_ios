//
//  FormViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//
import UIKit

class BFormViewController: UIViewController, MissionPageController, UITextViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    var missionId: Int!
    var missionPage: MissionPage!
    var elements: [PageElement]!
    var pagePosition: Int!
    var pageView: UIView!
    var watchRightAnswers: Bool! = false
    var watchUserAnswers: Bool! = false
    
    let placeholder = "Escribe tu respuesta aquí"
    
    @IBOutlet weak var BtableView: UITableView!
    
    var rows = [Int: BBaseRow]()
    
    var isEditingTextField = false
    
    var keyboardSize: CGRect!
    fileprivate var currentTextView: UIView!
    
    override func viewDidLoad() {
        
        self.BtableView.estimatedRowHeight = 500
        self.BtableView.rowHeight = UITableView.automaticDimension
       // self.BtableView.rowHeight = 69
        
        super.viewDidLoad()
        self.elements = missionPage.pageElements
        
        if(self.elements.count > 0){
            hideKeyboardWhenTappedAround()
            
            self.BtableView.allowsSelection = false
            
            self.rows = [Int: BBaseRow]()
            
            for e in 0...(self.elements.count-1){
                rows[e] = getCell(e)
            }
            
            self.BtableView.dataSource = self
            self.BtableView.delegate = self
           // self.BtableView.layoutMarginsDidChange()
            self.loadViewIfNeeded()
        }
        
    }
    
    func loadDefault() {
        if(self.elements.count > 0){
            for e in 0...(self.elements.count-1){
                rows[e] = getCell(e)
                rows[e]!.loadDefault()
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if isEditingTextField{
            self.dismissKeyboard()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(BFormViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BFormViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.loadViewIfNeeded()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if !isEditingTextField{
            isEditingTextField = true
            
            if let keyboardSize = ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                self.keyboardSize = keyboardSize
                if let ctv = currentTextView, self.shouldResizeScreen(ctv){
                    let oldY = self.view.frame.origin.y
                    
                    if oldY == 0{
                        self.pageView.frame.origin.y -= keyboardSize.height
                    }
                }
            }
        }
    }
    
    
    @objc func keyboardWillHide(_ notification: Notification) {
        isEditingTextField = false
        if ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue != nil {
            
            let oldY = self.pageView.frame.origin.y
            if oldY != 0{
                
                self.pageView.frame.origin.y = 0
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return missionPage.pageElements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let row = (indexPath as NSIndexPath).row
        print(row)
        let item: UITableViewCell = getCell(indexPath: indexPath)!
        
        item.layoutIfNeeded()
        item.layoutMarginsDidChange()
        item.reloadInputViews()
        return item
    }
    
    func  tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let index = (indexPath as NSIndexPath).row
        let pe = self.elements[index]
       // tableView.layoutIfNeeded()
        switch pe.QuestionType {
        case .Checkbox, .Radio:
            if index < self.rows.count{
                if let c = self.rows[index] as? BMultipleSelectRow{
                    c.pageElement = elements[(indexPath as NSIndexPath).row]
                    return c.getSize()!.height + 50
                }
            }
            return  tableView.rowHeight
            
            
        case .Image:
            return 150
            
        default:
            
            return tableView.rowHeight
        }
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    
   
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        self.currentTextView = textField
        
        
        return true
    }
    
//    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
//        if (textView.text! == placeholder) {
//            textView.text = "hey"
//            textView.textColor = UIColor.black
//            // textView.becomeFirstResponder()
//            //optional
//        }
//        return true
//    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
            if textView.textColor == UIColor.lightGray {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
            if textView.text.isEmpty {
                textView.text = placeholder
                textView.textColor = UIColor.black
            }
    }
    
    func isPageCompleted() -> Bool {
        if(watchUserAnswers){
            return true
        }
        
        let pes = self.elements
        if (pes?.count)! > 0{
            for i in 0...((pes?.count)! - 1){
                let pe = pes?[i]
                if let req = pe?.isRequired , req == true{
                    if pe?.isAnswered != true{
                        
                        
                        
                        
                        return false
                    }
                    
                }
            }
        }
        return true
    }
    
    func showErrorMessage(){
        UIUtils.toast("Hay campos requeridos no completados en esta página")
    }
    
    
    func isPageCorrect() -> Bool{
        
        let pes = self.missionPage!.pageElements
        var isCorrect = true
        if pes.count == 0 {
            return true
        }
        for i in 0...(pes.count - 1){
            let pe = pes[i]
            
            
            if let hasAnswer = pe.hasAnswer , hasAnswer == true{
                
                if !pe.isCorrectAndSaveAnswers(attempt: missionPage.attempts){
                    isCorrect = false
                }
                
            }
        }
        missionPage.attempts = missionPage.attempts + 1
        return isCorrect
        
    }
    
    func completeMissionPage() -> MissionPage{
        let listRows = Array(self.rows.values)
        for row in listRows{
            row.completePage()
        }
        return missionPage
    }
    
    func getCell(indexPath: IndexPath) -> BBaseRow?{
        let index = indexPath.row
       // print(index)
        if(index < 0){
            return nil
        }
        if(self.rows[index] != nil){
          //  print(Array(self.rows.keys))
            return self.rows[index]
        }
        
        let pe = self.elements[index]
        
        var item: BBaseRow?
        
        switch(pe.QuestionType){
            
        case .Select:
            let row = BtableView.dequeueReusableCell(withIdentifier: "BListSelectionRow", for: indexPath) as! BListSelectionRow
            row.pageElement = pe
            
            row.buttonPressed = {
                let optionMenu = row.getSelectOptionDialog()
                self.present(optionMenu, animated: true, completion: nil)
            }
            item = row
        case .Radio, .Checkbox:
            let row = BtableView.dequeueReusableCell(withIdentifier: "BMultipleSelectRow", for: indexPath) as! BMultipleSelectRow
            row.pageElement = pe
            row.hideKeyboard = self.dismissKeyboard
            row.watchRightAnswers = self.watchRightAnswers
            row.watchUserAnswers = self.watchUserAnswers
            row.tableView.reloadData()
            row.loadViews()
            
            item = row
            
        case .Input:
            let row = BtableView.dequeueReusableCell(withIdentifier: "BTextFieldRow", for: indexPath) as! BTextFieldRow
            row.pageElement = pe
            row.textField.delegate = self
            item = row
            
            
        case .Message:
            
            let row = BtableView.dequeueReusableCell(withIdentifier: "BMessageRow", for: indexPath) as! BMessageRow
            row.pageElement = pe
            
            row.labelView.text = pe.label!.htmlToString
            
           // row.labelView.attributedText = UIUtils.toNSAttributedString(htmlString: pe.label!, fontSize: 19)
            UIUtils.addShadowToCellView(row.labelContainerView, opacity: 0.2)
            
            item = row
            
        case .TextArea:
            let row = BtableView.dequeueReusableCell(withIdentifier: "BTextAreaRow", for: indexPath) as! BTextAreaRow
            row.pageElement = pe

            row.textView.delegate = self
            row.textView.isUserInteractionEnabled = true
            row.textView.isEditable = true
            row.textView.text = placeholder
            row.textView.textColor = UIColor.lightGray
            item = row
            
        case .Image:
            let row = BtableView.dequeueReusableCell(withIdentifier: "BImageRow", for: indexPath) as! BImageRow
            row.pageElement = pe
            row.imageButton.kf.setImage(with: URL(string: pe.mediaUrl!)!)
            row.imagePressed = self.showImageModal
            item = row
        
        case .Video:
            let row = BtableView.dequeueReusableCell(withIdentifier: "BVideoRow", for: indexPath) as! BVideoRow
            row.pageElement = pe
            row.controller = self
            row.loadVideo()
            item = row
            
            
        default:
            print("question type")
        }
        item?.isUserInteractionEnabled = !(self.watchRightAnswers || self.watchUserAnswers)
        self.rows[index] = item
        return item
    }
    
    
    
    func getCell(_ index: Int) -> BBaseRow?{
        let indexPath = IndexPath(row: index, section: 0)
        return getCell(indexPath: indexPath)
    }
    
    func showImageModal(_ imageView: UIImageView){
        if isEditingTextField{
            self.view.endEditing(true)
            return
        }
        let fullscreenController = self.storyboard!.instantiateViewController(withIdentifier: "FullscreenImageViewController") as! FullscreenImageViewController
        fullscreenController.image = imageView.image
        fullscreenController.modalFinished = {
            self.dismiss(animated: false, completion: nil)
        }
        self.present(fullscreenController, animated: false, completion: nil)
    }
    
    
    
    
    func shouldResizeScreen(_ rowView: UIView) -> Bool{
        let screenHeight = UIScreen.main.bounds.height
        //Esta variable muestra a que distancia esta la celda del textView desde el inicio de la tabla
        let rowPoint = rowView.convert(CGPoint(x: rowView.frame.midX, y: rowView.frame.midY) , to: pageView)
        if (rowPoint.y > screenHeight/2 - 20){
            return true
        }else{
            return false
        }
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(BFormViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}


extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
