

import UIKit
import MediaPlayer
import AVKit
import AVFoundation


class VideoViewController: BasePageViewController {
    var moviePlayer: BAVPlayerViewController!
    
    @IBOutlet weak var videoView: UIView!
    
    @IBOutlet weak var lblInfo: UILabel!
    
    var videoFinished: Bool = false
    var videoEnteredFullscreen = false;
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblInfo.text = self.missionPage.information
        
        if let videoUrl = self.missionPage.mediaUrl{
            let url: URL = URL(string: videoUrl)!

            let player = AVPlayer(url: url)
            moviePlayer = BAVPlayerViewController()
            moviePlayer.player = player
            moviePlayer.view.frame = videoView.bounds
            
            self.videoView.addSubview(moviePlayer.view)

           
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
       super.viewDidAppear(animated)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.moviePlayer.player?.pause()

        
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    
    func movieFinished(_ notification: Notification){
        self.videoFinished = false
    }
    
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
    
 
    
    func movieNaturalSizeAvailable(_ notification: Notification) {
        
    }
    

    
}
