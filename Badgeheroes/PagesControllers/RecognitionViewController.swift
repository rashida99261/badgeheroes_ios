//
//  RecognitionViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class RecognitionViewController: BasePageViewController {
    @IBOutlet weak var recognitionUserImage: UIImageView!
   
    @IBOutlet weak var recognitionTextField: UITextView!
    
    @IBOutlet weak var recognitionUserName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let u = self.missionPage.recognitionUser{
            self.recognitionUserImage.kf.setImage(with: URL(string: u.imageUrl!)!)
            self.recognitionUserName.text = u.FullName
            self.recognitionUserName.sizeToFit()
        }
        
        
        
        self.recognitionTextField.text = self.missionPage.recognitionText
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(RecognitionViewController.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RecognitionViewController.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    override func completeMissionPage() -> MissionPage {
        self.missionPage.recognitionText = self.recognitionTextField.text
        return self.missionPage
    }
    
    override func isPageCompleted() -> Bool {
        if let rt = self.missionPage.recognitionText{
            if (rt as NSString).length > 0{
                return true
            }
        }
        return false
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        
        
        if let keyboardSize = ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let oldY = self.view.frame.origin.y
            
            if oldY == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
            
        }
        
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        
        if ((notification as NSNotification).userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue != nil {
            
            let oldY = self.view.frame.origin.y
            if oldY != 0{
                self.view.frame.origin.y = 0
            }
            
        }
    }
   
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    
}
