//
//  IframeViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/26/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class IframeViewController:BasePageViewController {
    
    @IBOutlet weak var webView: UIWebView!
   // var campaign: Campaign?
    
    override func viewDidLoad() {
        
       // print(campaign?.Color)
       // self.view.backgroundColor = campaign?.Color
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if var url = self.missionPage.iframeUrl{
//            url = "https://jeronimopalacios.com/2016/10/scrumclinic-sprint-0-e-inception/"
            while url.hasSuffix("/"){
                url = url.substring(to: url.index(before: url.endIndex))
            }
            let url = URL(string: url)
            if let ur = url{
                self.webView.loadRequest(URLRequest(url: ur))
            }
            
        }
    }

   
}
