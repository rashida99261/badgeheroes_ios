//
//  FullscreenImageViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 6/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class FullscreenImageViewController: BModalViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    var image: UIImage?
    
    
    @IBOutlet weak var navBarView: UIView!
    
    override func viewDidLoad() {
        if let i = image{
            self.imageView.image = i
        }
        self.scrollView.isScrollEnabled = true
        self.scrollView.maximumZoomScale = 10
        self.scrollView.contentSize = self.imageView.frame.size
        self.navBarView.backgroundColor = BColor.PrimaryBar
        
    }
    
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.modalFinished?()
        
    }
    
    func viewForZoomingInScrollView(_ scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.all
    }
}
