//
//  ActivityViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/18/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftEventBus

class ActivityViewController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableFooterView : UIView!
    
    var logs: [HistoryLog] = [HistoryLog]()
    var generalLogs: [HistoryLog] = [HistoryLog]()
    var lastLogId = 0
    
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var navBarView: UIView!
    @IBOutlet weak var creditsLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    // Center Popup outlets
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var centerPopupVW: UIView!
    @IBOutlet weak var msgLabel: UILabel!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var centerImgView: UIImageView!
    
    @IBOutlet weak var generalesButton: UIButton!
    @IBOutlet weak var usuariosButton: UIButton!
    
    var userId: Int?
    
    var isBtnTapped = ""
    var unreadArray : [HistoryLog] = []
    var readArray : [HistoryLog] = []
    var readUserIdArray : [Int] = []
    var readGeneralIdArray : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.reloadLogo()
        tableFooterView.isHidden = true
        
        ///self.loadViewIfNeeded()
        //self.view.layoutIfNeeded()
        
        
        //self.tableView.layoutIfNeeded()
        //self.tableView.allowsSelection = false
        
        generalesButton.layer.borderColor = UIColor.darkGray.cgColor
        generalesButton.layer.borderWidth = 1
        usuariosButton.layer.borderColor = UIColor.darkGray.cgColor
        usuariosButton.layer.borderWidth = 1
        
        self.tableView.isHidden = true
        self.blurView.isHidden = true
        self.loader.startAnimating()
        
        SwiftEventBus.onMainThread(self, name: Events.LOGO_CHANGED.rawValue) { result in
            self.reloadLogo()
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        tableView.estimatedRowHeight = 91
        tableView.rowHeight = UITableView.automaticDimension
        
        UsersAPI.getProfile { (serverUser: User?, error: NSError?) in
            if let su = serverUser{
                self.loadUser(su)
                self.userId = su.id
                self.loader.stopAnimating()
                
                self.genralesClick(self.generalesButton)
            }
        }
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navBarView.backgroundColor = BColor.PrimaryBar
        GlobalBgColor = BColor.PrimaryBar
        
        //        if let tabBarController = (self.tabBarController as? TabBarController){
        //            tabBarController.setNavbarColor(nil)
        //            tabBarController.enableBackButton(false, navController: nil)
        //            tabBarController.setNavigationTitle(nil)
        //
        //        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(isBtnTapped == "user")
        {
            if (readUserIdArray.count > 0)
            {
                return 2
            }
        }
        else {
            if (readGeneralIdArray.count > 0)
            {
                return 2
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        if (section == 0) {
            return unreadArray.count
        }
        else {
            return readArray.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        let item = UITableViewCell()
        
        if(isBtnTapped == "user") {
            let item = tableView.dequeueReusableCell(withIdentifier: "BActivityCell", for: indexPath) as! BActivityCell
            
            let index: Int = (indexPath as NSIndexPath).row
            
            var log : HistoryLog!
            if (indexPath.section == 0) {
                log = unreadArray[index]
                
                if (index == unreadArray.count-1) {
                    //if let layer = item.contentView.layer.sublayers
                    // {
                    // layer[0].removeFromSuperlayer()
                    // }
                    //                   let border = CALayer()
                    //                    border.backgroundColor = UIColor.black.cgColor
                    //                    border.frame = CGRect(x:10, y: item.contentView.frame.height-2, width: item.contentView.frame.width-20, height:2)
                    //                    item.contentView.layer.addSublayer(border)
                }
            }
            else {
                log = readArray[index]
            }
            
            if (index % 2 == 0)
            {
                item.contentView.backgroundColor = UIColor.init(hexString: "F8F8F8")
            }
            else {
                item.contentView.backgroundColor = UIColor.white
            }
            if let user = log.user {
                
                let userMessage = log.Message
                
                let formattedString = NSMutableAttributedString()
                formattedString
                    .bold("\(user.FullName)")
                    .normal("\(userMessage)")
                item.userMessage.attributedText = formattedString
                
                //let userLabel = item.userName as! BUserNameLabel
                // userLabel.initLabel(user,delegateController:  self)
                // userLabel.modal = false
                
                let logImageView = item.logImage as! BItemImageView
                logImageView.initImage(log, delegateController: self)
                logImageView.modal = false
            }
            item.dateLabel.text = log.CreatedAt
            item.layoutIfNeeded()
            item.reloadInputViews()
            
            return item
        }
        else if(isBtnTapped == "general")
        {
            let item = tableView.dequeueReusableCell(withIdentifier: "BActivityCellGeneral", for: indexPath) as! BActivityCellGeneral
            
            let index: Int = (indexPath as NSIndexPath).row
            
            var log : HistoryLog!
            if (indexPath.section == 0){
                log = unreadArray[index]
                
                
                if (index == unreadArray.count-1) {
                    //if let layer = item.contentView.layer.sublayers
                    //  {
                    // layer[0].removeFromSuperlayer()
                    //  }
                    
                    //                    let border = CALayer()
                    //                    border.backgroundColor = UIColor.black.cgColor
                    //                    border.frame = CGRect(x:10, y: item.contentView.frame.height-2, width: item.contentView.frame.width-20, height:2)
                    //                    item.contentView.layer.addSublayer(border)
                }
            }
            else {
                log = readArray[index]
            }
            
            if (index % 2 == 0)
            {
                item.contentView.backgroundColor = UIColor.init(hexString: "F8F8F8")
            }
            else {
                item.contentView.backgroundColor = UIColor.white
            }
            
            if let _ = log.user {
                
                item.userMessage.text = log.Message
                
                //let userLabel = item.userName as! BUserNameLabel
                // userLabel.initLabel(user,delegateController:  self)
                // userLabel.modal = false
                
                let logImageView = item.logImage as! BItemImageView
                logImageView.initImage(log, delegateController: self)
                logImageView.modal = false
            }
            item.dateLabel.text = log.CreatedAt
            item.layoutIfNeeded()
            item.reloadInputViews()
            return item
        }
        return item
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (indexPath.section == 0) {
            let dictlog = unreadArray[indexPath.row]
            unreadArray.remove(at: indexPath.row)
            readArray.append(dictlog)
            
            if(isBtnTapped == "user") {
                readUserIdArray.append(dictlog.id!)
                UserDefaults.standard.set(readUserIdArray, forKey: "UserReadArr")
            }
            else {
                readGeneralIdArray.append(dictlog.id!)
                UserDefaults.standard.set(readGeneralIdArray, forKey: "GeneralReadArr")
            }
            UserDefaults.standard.synchronize()
            tableView.tableFooterView = tableFooterView
            tableView.reloadData()
        }
        //addGoToMissionTapAction?()
    }
    // var addGoToMissionTapAction : (()->())?
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = UIView.init(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 40))
        vw.backgroundColor = UIColor.white
        
        let lblLine = UILabel(frame: CGRect(x: 0, y: 1, width: vw.frame.width, height: 2))
        let lab = UILabel.init()
        lab.frame = vw.frame
        if (section == 0) {
            lab.text = "     Nuevas"
        }
        else {
            lab.text = "     Leidas"
            lblLine.backgroundColor = UIColor.darkGray
            vw.addSubview(lblLine)
        }
        vw.addSubview(lab)
        
        return vw
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        if (section == 0) && (unreadArray.count > 0)
        {
            tableFooterView.isHidden = true
            return tableFooterView
        }
        else if (section == 1) && (readArray.count > 0) {
            tableFooterView.isHidden = false
            return tableFooterView
        }
        return nil
        
        // recreate insets from existing ones in the table view
        //        let insets = tableView.separatorInset
        //        let width = tableView.bounds.width - insets.left - insets.right
        //        let sepFrame = CGRect(x: insets.left, y: 0, width: width, height: 1.5)
        //
        //        // create layer with separator, setting color
        //        let sep = CALayer()
        //        sep.frame = sepFrame
        //        sep.backgroundColor = UIColor.black.cgColor    //tableView.separatorColor
        //        result.layer.addSublayer(sep)
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        if (section == 0){
            return 2
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if (section == 0) && (unreadArray.count > 0)
        {
            return 40
        }
        else if (section == 1) && (readArray.count > 0) {
            return 40
        }
        return 0
    }
    
    @IBAction func leftMenuButtonPressed(_ sender: AnyObject) {
        self.slideMenuController()?.openLeft()
    }
    
    func loadUser(_ user: User){
        self.creditsLabel.text = "\(UIUtils.clean(user.credits))"
    }
    
    func reloadLogo(){
        UIUtils.loadLogo(logoImageView)
    }
    
    @IBAction func contactusClick(_ sender : UIButton)
    {
        blurView.isHidden = true
        self.viewDidAppear(true)
    }
    
    @IBAction func genralesClick(_ sender : UIButton)
    {
        isBtnTapped = "general"
        generalesButton.backgroundColor = UIColor.init(hexString: "F8F8F8")
        generalesButton.setTitleColor(UIColor.init(hexString: "646464"), for: .normal)
        usuariosButton.backgroundColor = UIColor.init(hexString: "E1E1E1")
        usuariosButton.setTitleColor(UIColor.init(hexString: "969696"), for: .normal)
        
        logs = []
        unreadArray = []
        readArray = []
        
        
        
        
        if let rddata = UserDefaults.standard.value(forKey: "GeneralReadArr")
        {
            let value = rddata as! [Int]
            readGeneralIdArray = value
        }
        
        LogsAPI.getHistoryLog(userId: self.userId!) { (logs: [HistoryLog]?, errors: NSError?) in
            
            if let serverLogs = logs {
                self.generalLogs = serverLogs
                
                for arr in self.generalLogs
                {
                    if (self.readGeneralIdArray.count > 0)
                    {
                        var chk : Bool = false
                        for rdid in self.readGeneralIdArray {
                            if (rdid == arr.id) {
                                chk = true
                                break
                            }
                        }
                        if (chk == false){
                            self.unreadArray.append(arr)
                        }
                        else {
                            self.readArray.append(arr)
                        }
                    }
                    else {
                        self.unreadArray = self.generalLogs
                    }
                }
                self.loader.stopAnimating()
                
                if(self.generalLogs.count > 0){
                    self.blurView.isHidden = true
                    self.generalesButton.isHidden = false
                    self.usuariosButton.isHidden = false
                    self.tableView.isHidden = false
                    self.tableView.reloadData()
                    if let last = self.generalLogs.last{
                        self.lastLogId = last.id!
                    }else{
                        self.lastLogId = Int.max
                    }
                }
                else{
                    self.tableView.isHidden = true
                    self.blurView.isHidden = false
                    self.generalesButton.isHidden = true
                    self.usuariosButton.isHidden = true
                }
               
            }
        }
    }
    
    @IBAction func usuariosClick(_ sender : UIButton)
    {
        isBtnTapped = "user"
        usuariosButton.backgroundColor = UIColor.init(hexString: "F8F8F8")
        usuariosButton.setTitleColor(UIColor.init(hexString: "646464"), for: .normal)
        generalesButton.backgroundColor = UIColor.init(hexString: "E1E1E1")
        generalesButton.setTitleColor(UIColor.init(hexString: "969696"), for: .normal)
        
        logs = []
        unreadArray = []
        readArray = []
        
        if let rddata = UserDefaults.standard.value(forKey: "UserReadArr")
        {
            let value = rddata as! [Int]
            readUserIdArray = value
        }
        
        LogsAPI.getPeopleHistoryLog(lastLogId: 0) { (logs: [HistoryLog]?, errors: NSError?) in
            if let serverLogs = logs {
                self.logs = serverLogs
                
                for arr in self.logs
                {
                    if (self.readUserIdArray.count > 0){
                        var chk : Bool = false
                        for rdid in self.readUserIdArray {
                            if (rdid == arr.id) {
                                chk = true
                                break
                            }
                        }
                        if (chk == false){
                            self.unreadArray.append(arr)
                        }
                        else {
                            self.readArray.append(arr)
                        }
                    }
                    else {
                        self.unreadArray = self.logs
                    }
                }
                
                self.blurView.isHidden = true
                self.generalesButton.isHidden = false
                self.usuariosButton.isHidden = false
                self.tableView.isHidden = false
                
                self.tableView.reloadData()
                if let last = self.logs.last{
                    self.lastLogId = last.id!
                }else{
                    self.lastLogId = Int.max
                }
                self.loader.stopAnimating()
            }
            else {
                self.tableView.isHidden = true
                self.blurView.isHidden = false
                self.generalesButton.isHidden = true
                self.usuariosButton.isHidden = true
            }
        }
    }
    
}

//1. Users Notification: (Users tabs)
//https://badgeheroes.herokuapp.com/api/logs/people. It shows information about all the users.



//2. General Notification: (General Tab)
//https://admin.badgeheroes.com/api/logs/users/<user_id>. It shows information about the specified user activity.

//new changes
extension ActivityViewController {
    
}
extension NSMutableAttributedString {
    @discardableResult func bold(_ text: String) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: 15)]
        let boldString = NSMutableAttributedString(string:text, attributes: attrs)
        append(boldString)
        
        return self
    }
    
    @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
        let normal = NSAttributedString(string: text)
        append(normal)
        
        return self
    }
}






