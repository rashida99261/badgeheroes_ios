//
//  LoginContactController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/25/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit


class TryDemoController: BModalViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var comments: UITextView!
    @IBOutlet weak var userName: BUITextField!
    var email: String = ""
    
    @IBOutlet weak var tryButton: BSecondaryButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.emailLabel.text = email
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.enableNavbar?(true)
        tryButton.backgroundColor = UIColor(hexString: "#ff8800")
    }
    
    @IBAction func acceptButtonPressed(_ sender: Any) {
        let email = emailLabel.text!
        let userName = self.userName.text!
        let comments = self.comments.text!
        tryButton.lockButton()
        UsersAPI.tryDemo(email: email, name: userName, comments: comments) { (response: ApiResponse?, error: NSError?) in
            if error != nil{
                UIUtils.toast("No se ha podido comunicar con el servidor")
                if let callback = self.modalFinished{
                    callback()
                }
                self.tryButton.unlockButton()
            }else{
                
                let em = "demo@badgeheroes.com"
                let pass = "demo"
                UsersAPI.performLogin(email: em, password: pass, callback: { (u: User?, e: NSError?) in
                    Settings.Email = em
                    if UIUtils.setApplicationUser(u: u, mf: self.modalFinished){
                        UIUtils.toast("¡Bienvenido a BadgeHeroes!")
                    }else{
                        UIUtils.toast("Contraseña incorrecta")
                    }
                    
                })
                
                
                
                
            }
        }
    }
    
    
}
