//
//  ActivityViewController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/18/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class AnswersViewController: BaseUIViewController, UITableViewDelegate, UITableViewDataSource {
    
    static let AnswersToMissionPagesSegue = "AnswersToMissionPagesSegue"
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblsin: UILabel!
    
    @IBOutlet weak var viewRejectPopUp: UIView!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    
    
    var answers: [Answer] = [Answer]()
    var mission: Mission!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadViewIfNeeded()
        self.view.layoutIfNeeded()
        
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        viewRejectPopUp.isHidden = true
        
        tableView.tableFooterView = UIView()
        self.tableView.layoutIfNeeded()
        self.tableView.allowsSelection = true
        
        self.loader.startAnimating()
        
        self.tableView.isHidden = true
        self.lblsin.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        MissionsAPI.getMissionAnswers(missionId: mission.id!) { (ans: [Answer]?, errors: NSError?) in
            if let answers = ans{
                self.answers = answers
                
                if(self.answers.count > 0){
                    self.tableView.isHidden = false
                    self.lblsin.isHidden = true
                    self.tableView.reloadData()
                }
                else{
                    self.tableView.isHidden = true
                    self.lblsin.isHidden = false
                }
                
                
                self.loader.stopAnimating()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("CELL PRESSED")
//        let answer = self.answers[indexPath.item]
//        if let m = self.mission{
//            MissionsAPI.getAnswer(missionId: m.id!, answerId: answer.id!, callback: { (mission, error) in
//                self.mission = mission
//                self.mission.assignAnswers()
//                self.performSegue(withIdentifier: AnswersViewController.AnswersToMissionPagesSegue, sender: self)
//            })
//        }
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
//        let answer = self.answers[indexPath.item]
//        if answer.State == Answer.AnswerState.Rejected{
//            return 78
//        }else{
//            return 80
//        }
        return 80
    }
    
   
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        var item: BAnswerCell
        let answer = self.answers[indexPath.item]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = answer.createdAt
        dateFormatter.dateFormat = "dd MMM yyyy"
        let strDate = dateFormatter.string(from: date!)
        
        if answer.State == Answer.AnswerState.Rejected{
            item = tableView.dequeueReusableCell(withIdentifier: "BRejectedAnswerCell", for: indexPath) as! BAnswerCell
            //item.comment.text = answer.comment
            item.btnVerMAs.tag = indexPath.item
            
           
            let strDespTitle =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-SemiBold", size: 16)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
            let myString = NSMutableAttributedString(string: "Vuelve a intentarlo ", attributes: strDespTitle)
            
            let strDescription =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-Regular", size: 14)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
            let strText = NSAttributedString(string: "\(strDate)", attributes: strDescription)
            myString.append(strText)
            item.dateLabel.attributedText = myString
            
            
            
            item.btnVerMAs.addTarget(self, action: #selector(clickOnVerBtn(_:)), for: UIControl.Event.touchUpInside)
            
        }else if answer.State == Answer.AnswerState.Accepted{
            item = tableView.dequeueReusableCell(withIdentifier: "BAcceptedAnswerCell", for: indexPath) as! BAnswerCell
            let strDespTitle =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-SemiBold", size: 16)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
            let myString = NSMutableAttributedString(string: "¡Tus respuestas fueron aprobadas! ", attributes: strDespTitle)
            
            let strDescription =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-Regular", size: 14)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
            let strText = NSAttributedString(string: "\(strDate)", attributes: strDescription)
            myString.append(strText)
            item.dateLabel.attributedText = myString
        }else{
            item = tableView.dequeueReusableCell(withIdentifier: "BWaitingAnswerCell", for: indexPath) as! BAnswerCell
            let strDespTitle =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-SemiBold", size: 16)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
            let myString = NSMutableAttributedString(string: "En espera de revisión... ", attributes: strDespTitle)
            
            let strDescription =  [NSAttributedString.Key.font: UIFont(name: "TitilliumWeb-Regular", size: 14)!,NSAttributedString.Key.foregroundColor: self.UIColorFromHex(rgbValue: 0x969898, alpha: 1.0)]
            let strText = NSAttributedString(string: "\(strDate)", attributes: strDescription)
            myString.append(strText)
            item.dateLabel.attributedText = myString
        }
        
        item.layoutIfNeeded()
        item.reloadInputViews()
        return item
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == AnswersViewController.AnswersToMissionPagesSegue{
            //let viewController = segue.destinationViewController as! ContainerPagesViewController
            let viewController = segue.destination as! BPagesViewController
            viewController.watchUserAnswersMode = true
            viewController.mission = self.mission
            
        }
    }
 
    @IBAction func clickOnBackBtn(_ sender : UIButton){
       self.dismiss(animated: true, completion: nil)
    }
    
    @objc func clickOnVerBtn(_ sender: UIButton)
    {
        self.viewRejectPopUp.isHidden = false
        let answer = self.answers[sender.tag]
        lblComments.text = answer.comment
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = answer.createdAt
        dateFormatter.dateFormat = "dd MMM yyyy"
        lblDate.text = dateFormatter.string(from: date!)
        
    }
    
    @IBAction func cliclOnRevisorBtn(_ sender : UIButton){
        self.viewRejectPopUp.isHidden = true
    }
    
}
