//
//  TabBarController.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 5/24/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var _enableBackButton: ((Bool, UINavigationController?) -> Void)?
    var _setNavigationTitle: ((String?) -> Void)?
    var _setNavbarColor: ((UIColor?) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadInputViews()
        self.delegate = self
        
        self.viewControllers?.forEach({ (viewController) in
            if let navController = viewController as? UINavigationController{
                navController.popToRootViewController(animated: false)
            }
        })
        
        self.selectPage(index: 0)
        countTabButtonPressed()
    }
    
    //Se llama cuando el controlador esta embebido
    override func willMove(toParent parent: UIViewController?) {
        
        if parent != nil{
            viewWillAppearForUser()
        }
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewWillAppearForUser()
    }
    
    
    fileprivate func selectPage(index: Int){
        self.selectedIndex = index
        if let controller = self.selectedViewController{
            controller.loadViewIfNeeded()
            controller.viewWillAppear(true)
            controller.viewDidAppear(true)
        }
    }
    
    
    
    fileprivate func viewWillAppearForUser(){
        //change tab bar color
        //self.tabBar.tintColor = UIColor(hexString: "#025579")
        //self.tabBar.barTintColor = UIColor(hexString: "#ffffff")
        self.tabBar.isTranslucent = false
        self.tabBar.barTintColor = BColor.PrimaryBar
    }
    
    func enableBackButton(_ bool: Bool,navController: UINavigationController?){
        if let function = _enableBackButton{
            function(bool, navController)
        }
    }
    
    func setNavigationTitle(_ title: String?){
        if let function = _setNavigationTitle{
            function(title)
        }
    }
    
    func setNavbarColor(_ color: UIColor?){
        if let function = _setNavbarColor{
            function(color)
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        countTabButtonPressed()
        
    }
    
    func countTabButtonPressed(){
        if let viewCode = BFirebase.ViewCode(rawValue: self.selectedIndex ){
            BFirebase.pushViewPressed(viewCode)
        }
    }
}
