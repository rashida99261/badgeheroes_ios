//
//  BFirebaseClickNotifier.swift
//  Badgeheroes
//
//  Created by Gonzalo Lopez on 8/8/16.
//  Copyright © 2016 MisPistachos. All rights reserved.
//

import Foundation
import RealmSwift

open class BFirebaseClickNotifier: BFirebaseNotifier {
    
    @objc dynamic var viewCode: Int = -1
    @objc dynamic var date: Int = -1
    
    fileprivate static var lastNotificationDate = 0
    
    override func notify(_ callback: (() -> Void)?) {
        
        let currentDate = self.date
        let apiViewCode = self.viewCode
        
        if viewCode != -1{
            
            //Enviar solo si han pasado 5 segundos desde la ultima notificacion
            if currentDate - BFirebaseClickNotifier.lastNotificationDate > 5{
                BFirebaseClickNotifier.lastNotificationDate = currentDate
                firebase ({ (ref) in
                    let path = "clients/\(Settings.ClientId)/clicks"
                    let autoKey = ref.child(path).childByAutoId().key
                    let click = ["uid": Settings.UserId,
                        "team_id": Settings.TeamId,
                        "date": currentDate,
                        "menu": apiViewCode]
                    let childUpdates = ["\(path)/\(String(describing: autoKey))": click]
                    ref.updateChildValues(childUpdates)
                    callback?()
                })
            }else{
                callback?()
            }
            
        }
        
        
    }
    
    
    
    public static func push(_ viewCode: Int, date: Date){
        let realm = try! Realm()
        let vc = viewCode
        let date = Int(date.timeIntervalSince1970)
        
        let clickNotifier = BFirebaseClickNotifier()
        clickNotifier.viewCode = vc
        clickNotifier.date = date
        try! realm.write({
            realm.add(clickNotifier)
        })
        
        let clickNotifies = realm.objects(BFirebaseClickNotifier.self)
        for cn in clickNotifies{
            cn.notify({
                try! realm.write {
                    realm.delete(cn)
                }
            })
        }

        
    }
    
}
